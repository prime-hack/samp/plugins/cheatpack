#include "main.h"
DWORD MenuKey;

bool MenuInit()
{
	static bool init = false;
	if (init) return false;

	D3DCOLOR color = 0xDD202040;

	MENU.gl = new Menu("CheatPack", -1, -1, 120, 130, color, -1, false);
	MENU.gl->BindCursor(true);

	NopsInit(color);
	ConnectsInit(color);
	ESPInit(color);
	CollisInit(color);
	PatchInit(color);
	mCharacterInit(color);
	OtherInit(color);

	MENU.gl->Tumblers->AddTumbler("����", 5, 5, MENU.nops->GetPresentPointer());
	MENU.gl->Tumblers->AddTumbler("�����������", 5, 20, MENU.connects->GetPresentPointer());
	MENU.gl->Tumblers->AddTumbler("���������", 5, 35, MENU.ESP->GetPresentPointer());
	MENU.gl->Tumblers->AddTumbler("��������", 5, 50, MENU.collis->GetPresentPointer());
	MENU.gl->Tumblers->AddTumbler("�����", 5, 65, MENU.patch->GetPresentPointer());
	MENU.gl->Tumblers->AddTumbler("�����", 5, 80, MENU.Character->GetPresentPointer());
	MENU.gl->Tumblers->AddTumbler("������", 5, 95, MENU.Other->GetPresentPointer());

	if (file_exist("SAMPFUNCS\\CheatPack\\bkg.png"))
	{
		stTextureInfo *Background = new stTextureInfo;
		Background = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\CheatPack\\bkg.png");
		MENU.gl->SetTextureBackground(Background);
		MENU.nops->SetTextureBackground(Background);
		MENU.connects->SetTextureBackground(Background);
		MENU.ESP->SetTextureBackground(Background);
			MENU.submenu.PickESP->SetTextureBackground(Background);
		MENU.collis->SetTextureBackground(Background);
		MENU.patch->SetTextureBackground(Background);
		MENU.Character->SetTextureBackground(Background);
		MENU.submenu.PlayerGM->SetTextureBackground(Background);
		MENU.submenu.CarGM->SetTextureBackground(Background);
		MENU.Other->SetTextureBackground(Background);
	}

	MenuKey = keyb.GetKeyNum(ini->esgString("����", "���������", "M"));

	return true;
}

void MenuEvents()
{
	if (SF->getGame()->isKeyPressed(MenuKey)) //��������� �������
	{
		DWORD MenuTopAddr = GetTopAddr();
		if (MenuTopAddr != NULL)
			if (IsInput(MenuTopAddr))
				return;
		MENU.gl->SetPresent(MENU.gl->IsPresent() ^ true);
	}
}