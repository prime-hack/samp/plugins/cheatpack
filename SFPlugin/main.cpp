#include "main.h"
SAMPFUNCS *SF = new SAMPFUNCS();
MENULIST MENU;
Nops *PacketNOP = new Nops;
Nops *RPCNOP = new Nops;
keyboard keyb;
stConnectInfo ConnectInfo;
stFontInfo *pFont = new stFontInfo;
IniFile *ini;
WeaponFunc weap;
WorldFunc world;
PlayerFunc player;
stGodMode GM;
stAditionalCharacterCheat ACC;
Notification *notif;
std::list<stKeyThumblerEvent> ThumblerEvent;
std::list<stKeyPatchesEvent> PathesEvent;
std::list<stKeyBindEvent> BindEvent;
stOtherFunc other;
stAdminCheckerInfo AdminCheckerInfo;
std::list<stKeyMenuCallbackEvent> MenuCallbackEvent;
char *szVersion = new char[64];
Updater *upd;

void CALLBACK Destructor()
{
	CollisDestruct(false);
	ESPDestruct(false);
	ConnectDestruct(false);
	mCharacterDestruct(false);
	OtherDestruct(false);

	PatchesDestruct();

	delete MENU.nops;
	delete MENU.gl;
}

void CALLBACK mainloop()
{
	static bool init = false;
	if (!init)
	{
		if (!SF->getSAMP()->IsInitialized())
			return;

		if (!MenuLoaded())
			return;

		sprintf(szVersion, "%s %s", __DATE__, __TIME__);
		ini = new IniFile("SAMPFUNCS\\CheatPack\\���������.ini", "");
		upd = new Updater("https://prime-hack.net/VersionCP.php", 1800000); //�������� ���������� ����� ��� � 30 �����

		MenuInit();

		InitTeleports();
		//SF->getGame()->registerGameDestructorCallback(Destructor);
		SF->getRakNet()->registerRakNetCallback(RakNetScriptHookType::RAKHOOK_TYPE_OUTCOMING_PACKET, outcomingData);
		SF->getRakNet()->registerRakNetCallback(RakNetScriptHookType::RAKHOOK_TYPE_INCOMING_PACKET, incomingData);
		SF->getRakNet()->registerRakNetCallback(RakNetScriptHookType::RAKHOOK_TYPE_INCOMING_RPC, incomingRPC);
		SF->getRakNet()->registerRakNetCallback(RakNetScriptHookType::RAKHOOK_TYPE_OUTCOMING_RPC, outcomingRPC);

		ScreenFont();
		SF->getRender()->registerD3DCallback( eDirect3DDeviceMethods::D3DMETHOD_PRESENT, Present );
		SF->getRender()->registerD3DCallback( eDirect3DDeviceMethods::D3DMETHOD_PRESENT, SkinESP );
		SF->getGame()->registerWndProcCallback(SF->getGame()->HIGH_CB_PRIORITY, WindowEvents);

		weap.loadAllWeaponModels();
		CmdInit();
		BindInit();
		AdminInit();

		int NotifMode = ini->esgInteger("�����������", "���", 1);
		int NotifLimit = ini->esgInteger("�����������", "���-�� �����", 5);
		float NotifX = ini->esgFloat("�����������", "X", 0.5) / 100.0f;
		float NotifY = ini->esgFloat("�����������", "Y", 33.33333) / 100.0f;
		POINT NotifPos = { *(int*)0xC9C040 * NotifX, *(int*)0xC9C044 * NotifY };
		notif = new Notification(pFont, NotifPos, NotifMode, NotifLimit);

		notif->AddNotification(0xFF40FF40, "CheatPack (%s) ������� �������� � ����� � �������������!", szVersion);

		init = true;
	}
	else
	{
		for ( int i = 0; i < g_listBots.size(); ++i )
			g_listBots[i]->Update();
		MenuEvents();
		RejTimer();
		MainCharacterEvents();
		upd->AutoChecker(); //����� ���� � ����� ��� ��������
	}
}

bool CALLBACK Present(CONST RECT *pSourceRect, CONST RECT *pDestRect, HWND hDestWindowOverride, CONST RGNDATA *pDirtyRegion)
{
	if (SUCCEEDED(SF->getRender()->BeginRender())) // ���� ������ ����� � ���������
	{
		if (!SF->getGame()->isGTAMenuActive())
		{
			//���������� ������
			SF->getRender()->getD3DDevice()->SetSamplerState(NULL, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);

			//������
			ESP();
			ClickWarp();
			RenderAdmins();
			MyActorInfo();
			notif->RenderNotification();
		}
		else if (SF->getSAMP()->getMisc()->iCursorMode)
			SF->getSAMP()->getMisc()->ToggleCursor(false);

		SF->getRender()->EndRender(); // ��������� ���������
	}
	return true;
};

bool CALLBACK WindowEvents(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	KeyEvent(msg, wParam);

	return true;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReasonForCall, LPVOID lpReserved)
{
	switch (dwReasonForCall)
	{
		case DLL_PROCESS_ATTACH:
			SF->initPlugin(mainloop, hModule);
			break;
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			Destructor();
			break;
	}
	return TRUE;
}

template<typename T>
inline void VectorErase( std::vector<T> &vec, T v )
{
	for ( int i = 0; i < vec.size(); ++i ){
		if ( vec[i] == v ){
			vec.erase( vec.begin() + i );
			break;
		}
	}
}