struct stConnectedPlayer
{
	int ID;
	std::string name;
};
struct stAdminCheckerInfo
{
	POINTFLOAT pos;
	std::list<std::string> admins_all;
	std::list<stConnectedPlayer> admins_connect;
	D3DCOLOR OutStream, InStream;
};
extern stAdminCheckerInfo AdminCheckerInfo;

void AdminInit();
void SortAdminList(stRakNetHookParams *param);
void RenderAdmins();