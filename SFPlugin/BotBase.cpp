#include "main.h"

std::vector<CBotBase*> g_listBots;

CBotBase::CBotBase()
{
	g_listBots.push_back( this );
}

CBotBase::~CBotBase()
{
	for ( int i = 0; i < g_listBots.size(); ++i ){
		if ( g_listBots[i] == this ){
			g_listBots.erase( g_listBots.begin() + i );
			break;
		}
	}
}

void CBotBase::Update()
{
	me = player.GetBonePosition( -1 );
	if ( wait > GetTickCount() )
		return;
	Step();
}

void CBotBase::Wait( DWORD time )
{
	wait = time + GetTickCount();
}

void CBotBase::Continue()
{
	wait = 0;
}

void CBotBase::RotateTo( CVector &vec )
{
	float angle = -world.GetAngleBetweenObjectsRad( me.fX, me.fY, vec.fX, vec.fY );
	CVector4D quat = player.GetQuaternion( -1 );
	quat.fX = cosf( angle / 2 );
	quat.fW = sinf( angle / 2 ); //Z
	player.SetQuaternion( quat );
}

bool CBotBase::GoTo( CVector &vec, float r )
{
	if ( world.dist2D( vec.fX, vec.fY, me.fX, me.fY ) < r )
		return true;

	SF->getGame()->emulateGTAKey( 1, -255 );
	if ( player.isDriver( -1 ) ){
		vehicle_info *veh = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle;
		veh->gas_pedal = 1.0f;
	}
	
	return false;
}

CVector CBotBase::GetMarker( float r, bool isRace )
{
	CVector ret = { 0.0, 0.0, 0.0 };
	DWORD MarkerPool = 0;
	for ( int i = 0; i < 32; ++i ){
		if ( isRace )
			MarkerPool = 0xC7F168 + i * 56;
		else MarkerPool = 0xC7DD88 + i * 160;
		CVector pos = *(CVector*)MarkerPool;
		if ( world.dist3D( me, pos ) < r ){
			r = world.dist3D( me, pos );
			ret = pos;
		}
	}
	return ret;
}

CVector CBotBase::Get3DText( float r, std::regex re, bool(CALLBACK*validator)(std::smatch&) )
{
	CVector ret = { 0.0, 0.0, 0.0 };
	for ( int i = 0; i < 2048; ++i ){
		if ( !SF->getSAMP()->getInfo()->pPools->pText3D->iIsListed[i] )
			continue;
		CVector pos = *(CVector*)SF->getSAMP()->getInfo()->pPools->pText3D->textLabel->fPosition;
		if ( pos.fX == 0.0 && pos.fY == 0.0 && pos.fZ == 0.0 )
			continue;
		std::string text = SF->getSAMP()->getInfo()->pPools->pText3D->textLabel->pText;
		std::smatch match;
		if ( world.dist3D( me, pos ) < r && std::regex_search(text, match, re)){
			if ( validator != nullptr )
				if ( !validator( match ) )
					continue;
			r = world.dist3D( me, pos );
			ret = pos;
		}
	}
	return ret;
}