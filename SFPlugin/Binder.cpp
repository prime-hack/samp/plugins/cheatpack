#include "main.h"

void BindInit()
{
	IniFile *binder = new IniFile("SAMPFUNCS\\CheatPack\\Binder.ini", "", false);

	std::vector <std::string> bind = binder->getArrayString("MESSAGE", "BIND");
	ReadBinds(bind, binder, false);

	//TARGET
	bind = binder->getArrayString("TARGET", "BIND");
	ReadBinds(bind, binder, true);

	delete binder;
}

void ReadBinds(std::vector<std::string> &bind, IniFile *binder, bool target)
{
	for (int i = 0; i < bind.size(); i++)
	{
		std::vector <std::string> params = binder->getParams(bind[i]);
		if (params.size() != 2) continue; // key, message

		stKeyBindEvent stBind;
		stBind.key = keyb.GetKeyNum(params[0]);
		stBind.message = params[1];
		stBind.target = target;

		BindEvent.push_front(stBind);
	}
}