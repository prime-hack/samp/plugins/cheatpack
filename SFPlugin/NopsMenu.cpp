#include "main.h"
void CALLBACK NopsListEvent(int Select, byte key)
{ //�������� �� �������� ���������, �� ����� ��� ����� ������� �� ���������. ���� ���������� ������ ��� ����������� (���� � ����, ��� �� �� �����).
	SF->Log("������! �� ��������� ��� \"%s\". ���� �� �� �������� ������� ���� ��������� � ��� � �� ������� ��������� ������, �� ���������� �������� �� ������ �� ������.", MENU.nops->Listings->GetTextOnListingByID(1, Select));
}

void NopsInit(D3DCOLOR &color)
{
	MENU.nops = new Menu("����", -1, -1, 500, 210, color, -1, false);
	MENU.nops->SetParent(MENU.gl);
	MENU.nops->Listings->AddListing(5, 5, 245, 180, NopsListEvent);
	MENU.nops->Listings->AddListing(255, 5, 245, 180, NopsListEvent);

	IniFile *nop = new IniFile("SAMPFUNCS\\CheatPack\\NOPS.ini", "", false);


	//Packets
	memset(PacketNOP->Incomming, 0, 256);
	memset(PacketNOP->Outcomming, 0, 256);
	std::vector <std::string> packets = nop->getArrayString("Packets", "NOP");

	for (int i = 0; i < packets.size(); i++)
	{
		std::vector <std::string> params = nop->getParams(packets[i]);
		if (params.size() != 5) continue; // In/Out, number, name, autorun

		bool preset = false;
		if (!strcmp("true", (char*)convToLowercase((char*)params[3].c_str()).c_str())
			|| !strcmp("1", (char*)convToLowercase((char*)params[3].c_str()).c_str()))
			preset = true;

		char name[64];
		sprintf(name, "%s: %s", params[0].c_str(), params[2].c_str());

		stKeyThumblerEvent KEYEVENT;
		KEYEVENT.name = name;
		KEYEVENT.key = keyb.GetKeyNum(params[4]);

		if (strcmp("in", (char*)convToLowercase((char*)params[0].c_str()).c_str()) == 0)
		{
			MENU.nops->Listings->AddTumblerOnListing(0, name, &PacketNOP->Incomming[atoi(params[1].c_str())]);
			PacketNOP->Incomming[atoi(params[1].c_str())] = preset;

			KEYEVENT.bVar = &PacketNOP->Incomming[atoi(params[1].c_str())];
			ThumblerEvent.push_back(KEYEVENT);
		}
		else
		{
			MENU.nops->Listings->AddTumblerOnListing(0, name, &PacketNOP->Outcomming[atoi(params[1].c_str())]);
			PacketNOP->Outcomming[atoi(params[1].c_str())] = preset;

			KEYEVENT.bVar = &PacketNOP->Outcomming[atoi(params[1].c_str())];
			ThumblerEvent.push_back(KEYEVENT);
		}
	}


	//RPC
	memset(RPCNOP->Incomming, 0, 256);
	memset(RPCNOP->Outcomming, 0, 256);
	std::vector <std::string> RPC = nop->getArrayString("RPC", "NOP");

	for (int i = 0; i < RPC.size(); i++)
	{
		std::vector <std::string> params = nop->getParams(RPC[i]);
		if (params.size() != 5) continue; // In/Out, number, name, autorun

		bool preset = false;
		if (!strcmp("true", (char*)convToLowercase((char*)params[3].c_str()).c_str())
			|| !strcmp("1", (char*)convToLowercase((char*)params[3].c_str()).c_str()))
			preset = true;

		char name[64];
		sprintf(name, "%s: %s", params[0].c_str(), params[2].c_str());

		stKeyThumblerEvent KEYEVENT;
		KEYEVENT.name = name;
		KEYEVENT.key = keyb.GetKeyNum(params[4]);

		if (strcmp("in", (char*)convToLowercase((char*)params[0].c_str()).c_str()) == 0)
		{
			MENU.nops->Listings->AddTumblerOnListing(1, name, &RPCNOP->Incomming[atoi(params[1].c_str())]);
			RPCNOP->Incomming[atoi(params[1].c_str())] = preset;

			KEYEVENT.bVar = &RPCNOP->Incomming[atoi(params[1].c_str())];
			ThumblerEvent.push_back(KEYEVENT);
		}
		else
		{
			MENU.nops->Listings->AddTumblerOnListing(1, name, &RPCNOP->Outcomming[atoi(params[1].c_str())]);
			RPCNOP->Outcomming[atoi(params[1].c_str())] = preset;

			KEYEVENT.bVar = &RPCNOP->Incomming[atoi(params[1].c_str())];
			ThumblerEvent.push_back(KEYEVENT);
		}
	}
	delete nop;
}