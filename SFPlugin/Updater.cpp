#include "main.h"

Updater::Updater(std::string URL, DWORD dwTime)
{
	this->URL = URL;
	bSyncThread = true;
	if (dwTime > 60000)
		dwCheckTime = dwTime;
}

bool Updater::CheckUpdate()
{
	if (!bSyncThread) //��� �� ������ ���������� ��������.
		return false;

	bSyncThread = false;
	_beginthread((void(__cdecl *)(void *))NetThread, 2048, this);

	return true; //�������� ������.
}

void Updater::AutoChecker()
{
	static DWORD dwTime = 0;
	if (dwTime < GetTickCount())
	{
		if (CheckUpdate())
			dwTime = dwCheckTime + GetTickCount();
	}
}

void Updater::NetThread(Updater *th) //������� �����. ���� ��� �������� 2��
{
	HINTERNET hInternet = 0, hRequest; //��������� ����� ����������� � ���� ������� "CheatPack"
	hInternet = InternetOpenA("CheatPack", 0, 0, 0, 0);
	if (!hInternet)
	{
		notif->AddNotification(0xFFFFCC40, "������ ����������. �� ������� ������� �����������");
		return;
	}

	hRequest = InternetOpenUrlA(hInternet, th->URL.c_str(), 0, 0, 0, 0); //���������� �� ������
	if (!hRequest)
	{
		notif->AddNotification(0xFFFFCC40, "������ ����������. �� ������� ��������� � ��������");
		return;
	}

	if (!HttpSendRequestA(hRequest, NULL, 0, NULL, 0))
	{
		notif->AddNotification(0xFFFFCC40, "������ ����������. ������ �� ����������� �����");
		return;
	}

	//������ ����� �� �������
	char szData[64];
	memset(szData, 0, sizeof(szData));
	DWORD dwBytesRead;
	BOOL bRead = InternetReadFile( hRequest, szData, sizeof(szData) - 1, &dwBytesRead);
	if (dwBytesRead < 64)
		szData[dwBytesRead] = 0;

	if (!InternetCloseHandle(hInternet))
	{
		notif->AddNotification(0xFFFFCC40, "������ ����������. �� ������� ������� ����������");
		return;
	}

	//���������� ����� � ������� ������� �������
	if (strcmp(szVersion, szData) != NULL)
		notif->AddNotification(0xFFFFCC40, 20000, "�������� ���������� �� %s", szData);

	th->bSyncThread = true; //�������������� ����� � �������, �������� ����� ����� �������� �� ����������.
}