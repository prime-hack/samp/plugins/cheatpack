bool CALLBACK outcomingData(stRakNetHookParams *params);
bool CALLBACK incomingData(stRakNetHookParams *params);
bool CALLBACK outcomingRPC(stRakNetHookParams *params);
bool CALLBACK incomingRPC(stRakNetHookParams *params);