class Queue
{
	LPVOID data;
public:
	Queue* next;
	Queue* prev;
	Queue();
	~Queue();
	void push(Queue *after, LPVOID data);
	LPVOID pop();
	bool IsEmpty();
};

