class WorldFunc
{
public:
	float dist3D(CVector One, CVector Two);
    CVector GetCameraTartget();
	CVector GetCameraPosition();
	bool OnScreen(CVector &vect, float range = 0.0); //CLEO
	bool GTAfunc_IsLineOfSightClear(CVector *vecStart, CVector *vecEnd, bool bCheckBuildings, bool bCheckVehicles, bool bCheckPeds, bool bCheckObjects, bool bCheckDummies, bool bSeeThroughStuff, bool bIgnoreSomeObjectsForCamera);
	float GetAngleBetweenObjectsRad(float &x1, float &y1, float &x2, float &y2);
	float dist2D(float x1, float y1, float x2, float y2);
	CVector MarkerPos();
	float FindGroundZForPosition(float &fX, float &fY);
	float FindGroundZFor3DPosition(CVector * vecPosition);
	int FindCar(CVector &Pos, bool Empty = false);
	void PosUpdate(CVector POS);
	std::string get_model_name_by_id(int ModelID);
};
extern WorldFunc world;
