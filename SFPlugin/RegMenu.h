struct SUBMENULIST
{
	Menu* PlayerGM;
	Menu* CarGM;
	Menu* PickESP;
};

struct MENULIST
{
	Menu* gl;
	Menu* nops;
	Menu* connects;
	Menu* ESP;
	Menu* collis;
	Menu* patch;
	Menu* Character;
	Menu* Other;
	SUBMENULIST submenu;
};
extern MENULIST MENU;
#include "NopsMenu.h"
#include "Connects.h"
#include "ESP.h"
#include "CollisionMenu.h"
#include "PatchMenu.h"
#include "MainCharacterMenu.h"
#include "OtherMenu.h"

bool MenuInit();

void MenuEvents();