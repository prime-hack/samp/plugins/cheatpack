#pragma once

class CBotBase;
extern std::vector<CBotBase*> g_listBots;

class CBotBase
{
public:
	CBotBase();
	virtual ~CBotBase();

	virtual void Update();
	virtual void Wait( DWORD time );
	virtual void Continue();

protected:
	CVector me;

	virtual void Step() = 0;

	void RotateTo( CVector &vec );
	bool GoTo( CVector &vec, float r = 0.5 );
	CVector GetMarker( float r = 50.0f, bool isRace = false );
	CVector Get3DText( float r = 50.0f, std::regex re = std::regex(R"(.*)"),
					   bool(CALLBACK*validator)(std::smatch&) = nullptr );

private:
	DWORD wait;
};

/*
 * Car control 0xB73458
 * 
 * 0x20 - gas
 * 0x50 - gas
 * 
 * 0x1C - break
 * 0x4C - break
 *
 **/