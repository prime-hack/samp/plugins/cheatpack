#include "main.h"

float WorldFunc::dist3D(CVector One, CVector Two)
{
	One.fX -= Two.fX;
	One.fY -= Two.fY;
	One.fZ -= Two.fZ;
	return sqrt((One.fX*One.fX) + (One.fY*One.fY) + (One.fZ*One.fZ));
};

CVector WorldFunc::GetCameraPosition()
{
	return *(CVector *)(0x00B6F338);
};

CVector WorldFunc::GetCameraTartget()
{
	return *(CVector *)(0x00B6F338 - 0x0C);
};

bool WorldFunc::OnScreen(CVector &vect, float range)
{
	SF->getCLEO()->callOpcode("00C2: %f %f %f %f", vect.fX, vect.fY, vect.fZ, range);
	return SF->getCLEO()->GetCondResult();
}

bool WorldFunc::GTAfunc_IsLineOfSightClear(CVector *vecStart, CVector *vecEnd, bool bCheckBuildings, bool bCheckVehicles, bool bCheckPeds, bool bCheckObjects, bool bCheckDummies, bool bSeeThroughStuff, bool bIgnoreSomeObjectsForCamera)
{
	DWORD	dwFunc = 0x56A490;
	bool	bReturn = false;
	__asm
	{
		push bIgnoreSomeObjectsForCamera
			push bSeeThroughStuff
			push bCheckDummies
			push bCheckObjects
			push bCheckPeds
			push bCheckVehicles
			push bCheckBuildings
			push vecEnd
			push vecStart
			call dwFunc
			mov bReturn, al
			add esp, 0x24
	}

	return bReturn;
}

float WorldFunc::GetAngleBetweenObjectsRad(float &x1, float &y1, float &x2, float &y2)
{
	float kx, ky;
	float t, a;

	kx = x2 - x1;
	ky = y2 - y1;
	if (kx == 0) kx = 0.00001f;
	t = kx / ky;
	if (t < 0) t = -t;

	a = (float)(180 * atan((float)t) / M_PI);

	if ((kx <= 0) && (ky <= 0)) a = 180 - a;
	else if ((kx >= 0) && (ky >= 0)) a = 359.99999f - a;
	else if ((kx >= 0) && (ky <= 0)) a = -(180 - a);

	a = (a * M_PI) / 180.0f;

	return a;
}

float WorldFunc::dist2D(float x1, float y1, float x2, float y2)
{
	x1 -= x2;
	y1 -= y2;
	return sqrt((x1*x1) + (y1*y1));
};

CVector WorldFunc::MarkerPos()
{
	CVector	mapPos;
	for (int i = 0; i < (0xAF * 0x28); i += 0x28)
	{
		if (*(short *)(0xBA873D + i) == 4611)
		{
			float	*pos = (float *)(0xBA86F8 + 0x28 + i);
			mapPos.fX = *pos;
			mapPos.fY = *(pos + 1);
			mapPos.fZ = FindGroundZForPosition(mapPos.fX, mapPos.fY) + 2.0f;
			delete pos;
		}
	}
	return mapPos;
}

float WorldFunc::FindGroundZForPosition(float &fX, float &fY)
{
	DWORD dwFunc = 0x569660; //FUNC_FindGroundZForCoord
	FLOAT fReturn = 0;
	_asm
	{
		push	fY
		push	fX
		call	dwFunc
		fstp	fReturn
		add		esp, 8
	}
	return fReturn;
};

float WorldFunc::FindGroundZFor3DPosition(CVector * vecPosition)
{
	DWORD dwFunc = 0x5696C0; //FUNC_FindGroundZFor3DCoord
	FLOAT fReturn = 0;
	FLOAT fX = vecPosition->fX;
	FLOAT fY = vecPosition->fY;
	FLOAT fZ = vecPosition->fZ;
	_asm
	{
		push	0
			push	0
			push	fZ
			push	fY
			push	fX
			call	dwFunc
			fstp	fReturn
			add		esp, 0x14
	}
	return fReturn;
}

int WorldFunc::FindCar(CVector &Pos, bool Empty)
{
	float dist = 1337228.1488;
	int ID = -1;
	uint32_t P00L = *(int*)((*(int*)0xB74494) + 0x4);
	for (uint32_t hCount = 0; hCount < 35584; hCount += 0x100)
	{
		uint32_t VEHICLE = *(BYTE*)P00L;
		P00L++;
		if ((VEHICLE >= 0x00) && (VEHICLE < 0x80))
		{
			VEHICLE += hCount;
			VEHICLE = SF->getSAMP()->getVehicles()->GetSAMPVehicleIDFromGTAHandle(VEHICLE);

			if (Empty && SF->getSAMP()->getVehicles()->pGTA_Vehicle[VEHICLE]->passengers[0] != NULL) continue;

			int mem = (*(int*)(((int)SF->getSAMP()->getVehicles()->pGTA_Vehicle[VEHICLE]) + 20)) + 48;
			CVector CarPos = *(CVector*)mem;

			if (dist3D(Pos, CarPos) < dist)
			{
				dist = dist3D(Pos, CarPos);
				ID = VEHICLE;
			}
		}
	}
	return ID;
}

void WorldFunc::PosUpdate(CVector POS)
{
	if (player.Driving(ACTOR_SELF))
	{
		stInCarData data;
		memset(&data, 0, sizeof(stInCarData));
		data = SF->getSAMP()->getPlayers()->pLocalPlayer->inCarData;

		data.fPosition[0] = POS.fX;
		data.fPosition[1] = POS.fY;
		data.fPosition[2] = POS.fZ;

		BitStream sync;
		sync.Write((BYTE)ID_VEHICLE_SYNC);
		sync.Write((PCHAR)&data, sizeof(stInCarData));
		SF->getRakNet()->SendPacket(&sync);
	}
	else
	{
		stOnFootData data;
		memset(&data, 0, sizeof(stOnFootData));
		data = SF->getSAMP()->getPlayers()->pLocalPlayer->onFootData;

		data.fPosition[0] = POS.fX;
		data.fPosition[1] = POS.fY;
		data.fPosition[2] = POS.fZ;

		BitStream sync;
		sync.Write((BYTE)ID_PLAYER_SYNC);
		sync.Write((PCHAR)&data, sizeof(stOnFootData));
		SF->getRakNet()->SendPacket(&sync);
	}
}

std::string WorldFunc::get_model_name_by_id(int ModelID)
{
	if (ModelID >= 259 && ModelID <= 294) return "������";
	switch (ModelID)
	{
	case 119:
		return "�������";
	case 353:
		return "��5";
	case 954:
		return "�������";
	case 974:
		return "�������� �����";
	case 1210:
		return "���������";
	case 1212:
		return "������";
	case 1213:
		return "??? 1";
	case 1239:
		return "����������";
	case 1240:
		return "��������";
	case 1241:
		return "�������";
	case 1242:
		return "�����";
	case 1247:
		return "������";
	case 1248:
		return "GTA III";
	case 1252:
		return "??? 2";
	case 1253:
		return "�����";
	case 1254:
		return "DM";
	case 1272:
		return "�������� ���";
	case 1273:
		return "��������� ���";
	case 1274:
		return "������";
	case 1275:
		return "������";
	case 1276:
		return "����� �� VC";
	case 1277:
		return "�������";
	case 1279:
		return "������";
	case 1310:
		return "�������";
	case 1313:
		return "TDM";
	case 1314:
		return "������";
	case 1318:
		return "���������";
	case 1550:
		return "����� � ��������";
	case 1575:
		return "���������� �����";
	case 1576:
		return "��������� �����";
	case 1577:
		return "������ �����";
	case 1578:
		return "������� �����";
	case 1579:
		return "����� �����";
	case 1580:
		return "������� �����";
	case 1581:
		return "�����";
	case 1582:
		return "�����";
	case 1636:
		return "������";
	case 1644:
		return "������ �������";
	case 1650:
	case 1651:
		return "��������";
	case 1654:
		return "�������";
	case 1666:
		return "���� ����";
	case 1672:
		return "������� �������";
	case 2035:
		return "�4";
	case 2036:
		return "���������";
	case 2037:
		return "??? 3";
	case 2044:
		return "��5";
	case 2045:
		return "���� � ��������";
	case 2057:
		return "������������";
	case 2060:
		return "�����";
	case 2061:
		return "���������";
	case 2228:
	case 2237:
		return "������";
	case 2690:
		return "������������";
	case 2709:
		return "��������";
	case 2710:
		return "�������� ����";
	case 18778:
	case 1655:
	case 13641:
		return "��������";
	case 3363:
		return "���-��������";
	case 16304:
		return "����";
	case 7073:
		return "������� �������";
	case 1225:
		return "�����";
	case 7392:
		return "������� �������";
	case 13592:
		return "�������� ������";
	case 18647:
		return "������� ���������";
	case 18648:
		return "����� ���������";
	case 18649:
		return "������� ���������";
	case 18650:
		return "������ ���������";
	case 18651:
		return "��������� ���������";
	case 18652:
		return "����� ���������";
	case 18728:
		return "�����";
	case 18750:
		return "���� SA:MP";
	case 19076:
		return "���������� ����";
	case 19523:
		return "����������� ���";
	case 19130:
		return "���������-�����";
	case 19132:
		return "���������-�����";
	case 19135:
		return "���������-�����";
	default:
		return " ";
	}
};
