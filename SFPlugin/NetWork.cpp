#include "main.h"
bool CALLBACK outcomingData(stRakNetHookParams *params)
{
	if (params->packetId == ID_REMOTE_DISCONNECTION_NOTIFICATION ||
		params->packetId == ID_REMOTE_CONNECTION_LOST)
		ConnectInfo.state = 0;

	if (params->packetId == ID_PLAYER_SYNC)
	{
		stOnFootData data; // ���������� ������ � ������� �������� ������������ ������
		memset(&data, 0, sizeof(stOnFootData)); // ��������
		params->bitStream->ResetReadPointer(); // �� ������ ������ ������������� ������ ������ �� ������
		params->bitStream->IgnoreBits(8); //������� ����� ������
		params->bitStream->Read((PCHAR)&data, sizeof(stOnFootData)); // ������ ������������ ������

		BunnyHop(data);
		DMZZ(data);

		params->bitStream->ResetWritePointer(); // �������� ������ ������
		params->bitStream->Write((byte)ID_PLAYER_SYNC); // ����� �� ������
		params->bitStream->Write((PCHAR)&data, sizeof(stOnFootData)); // ����� ����������� ������
	}

	if ( params->packetId == ID_VEHICLE_SYNC )
	{
		stInCarData data; // ���������� ������ � ������� �������� ������������ ������
		memset( &data, 0, sizeof( stInCarData ) ); // ��������
		params->bitStream->ResetReadPointer(); // �� ������ ������ ������������� ������ ������ �� ������
		params->bitStream->IgnoreBits( 8 ); //������� ����� ������
		params->bitStream->Read( (PCHAR)&data, sizeof( stInCarData ) ); // ������ ������������ ������

		FireCar( data );

		params->bitStream->ResetWritePointer(); // �������� ������ ������
		params->bitStream->Write( (byte)ID_VEHICLE_SYNC ); // ����� �� ������
		params->bitStream->Write( (PCHAR)&data, sizeof( stInCarData ) ); // ����� ����������� ������
	}

	if (params->packetId == ID_BULLET_SYNC)
	{
		BugC();
	}

	if (PacketNOP->Outcomming[params->packetId])
		return false;
	return true;
}
bool CALLBACK incomingData(stRakNetHookParams *params)
{
	switch (params->packetId)
	{
	case ID_CONNECTION_REQUEST_ACCEPTED:
		ConnectInfo.state = 1;
		break;
	case ID_DISCONNECTION_NOTIFICATION:
		ConnectInfo.state = 2;
			Reconnect(1337);
		break;
	case ID_CONNECTION_BANNED:
		ConnectInfo.state = 3;
			Reconnect(1337);
		break;
	case ID_CONNECTION_LOST:
		ConnectInfo.state = 4;
			Reconnect(1337);
		break;
	default:
		break;
	}

	if (PacketNOP->Incomming[params->packetId])
		return false;
	return true;
}
bool CALLBACK outcomingRPC(stRakNetHookParams *params)
{
	if (RPCNOP->Outcomming[params->packetId])
		return false;
	return true;
}
bool CALLBACK incomingRPC(stRakNetHookParams *params)
{
	ESP_NickShow(params);
	SortAdminList(params);

	if (RPCNOP->Incomming[params->packetId])
		return false;

	if (params->packetId == ScriptRPCEnumeration::RPC_ScrSetPlayerHealth)
	{
		float SetHealth = 100.0;
		params->bitStream->ResetReadPointer();
		if (params->bitStream->GetNumberOfBytesUsed() > 4)
			params->bitStream->IgnoreBits((params->bitStream->GetNumberOfBytesUsed() - 4) * 8);
		params->bitStream->Read(SetHealth); // Id ������
		if (SetHealth > 255.0f)
			SetHealth = 255.0;
		if (SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE) != nullptr)
			SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->hitpoints = SetHealth;

		return false;
	}
	else if ( other.DestoryCar && params->packetId == RPC_ScrRemovePlayerFromVehicle ){
		int ID = player.GetCarID( -1 );
		if ( ID == -1 )
			return false;
		if ( SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID] == nullptr )
			return false;
		if ( SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID]->pGTA_Vehicle == nullptr )
			return false;
		SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID]->pGTA_Vehicle->hitpoints = 235.0f;
		stInCarData data = { 0 };
		*(CVector*)data.fMoveSpeed = CVector( 0.0, 0.0, 0.0 );
		if ( SF->getSAMP()->getVehicles()->pGTA_Vehicle[ID] != 0 ){
			int posOffset = (*(int*)(((int)SF->getSAMP()->getVehicles()->pGTA_Vehicle[ID]) + 20)) + 48;
			*(CVector*)data.fPosition = *(CVector*)posOffset;
		}
		data.byteArmor = GAME->GetPools()->GetPedFromRef( 1 )->GetArmor();
		data.bytePlayerHealth = GAME->GetPools()->GetPedFromRef( 1 )->GetHealth();
		data.fVehicleHealth = 230.0f;
		data.sVehicleID = ID;

		BitStream carsync;
		carsync.Write( (BYTE)ID_VEHICLE_SYNC ); // ���������� ID ������.
		carsync.Write( (PCHAR)&data, sizeof( stInCarData ) ); // ���������� ������ �� ��������� sync
		SF->getRakNet()->SendPacket( &carsync ); // ���������� ����� �� ������.
	}

	return true;
}