void ESPInit(D3DCOLOR &color);
void ESPDestruct(bool SaveOnly = true);
void MyActorInfo();
void ESP();
void WeaponInfo(int &ID);
void DrawNicks(int &ID);
void ESP_NickShow(stRakNetHookParams *param);
void DrawEspBox(int &ID);
void EasyESP(int &ID);

typedef struct stEspBox stEspBox;
struct stEspBox
{
	int left, right, up, down;
};
stEspBox GetPlayerBox(int &ID);

struct stESP_cheats
{
	bool WeaponInfo = false;
	bool DrawNicks = false;
	bool ActorInfo = false;
	bool TrueEspBox = false;
	bool EasyEspBox = false;
	bool PlayerShow[1004];
	bool MiniMap = false;
	bool skin = false;
};

struct stMiniMap
{
	bool Changed = false;
	D3DCOLOR origClr;
};
void MiniMAP(int &ID);

struct stPickups
{
	bool bName = false;
	bool bID = false;
	bool bModel = false;
	float fDist = 50.0;
};
void EspPick(D3DCOLOR &color);
void PickUpEsp();
mcall ApplyPickDist(int KeyClicked);
bool CALLBACK SkinESP( CONST RECT *pSourceRect, CONST RECT *pDestRect, HWND hDestWindowOverride, CONST RGNDATA *pDirtyRegion );