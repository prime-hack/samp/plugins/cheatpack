#include "main.h"

void RegisterCommand(std::string command, CommandProc callback, std::string description = "")
{
	bool initialize = false;
	bool regChat = true;
	if (!initialize){
		regChat = ini->esgBoolean("�������", "�������������� � ����", true, true);
		initialize = true;
	}
	if (regChat)
		SF->getSAMP()->registerChatCommand(command, callback);
	SF->registerConsoleCommand(command, callback);

	if (description != "") //���� ����� �� �������, �� ������ �� 2 ���������� �����������
		SF->setConsoleCommandDescription(command, description);
}

bool CmdInit()
{
	static bool init = false;
	if (init) return false;

	RegisterCommand("gun", cmd_gun, "������ ��������� ������ � ��������� ����������� ������.\n���� �� ������� �������, �� ����� ������ 250 ������.");
	RegisterCommand("warp", cmd_warp, "������������� ��� � ���������� ������.");
	RegisterCommand( "gp", cmd_gp, "��������� ��������� �����" );
	RegisterCommand( "dcar", cmd_desCar, "��������� ������ �� �� id" );

	init = true;
	return true;
}

void CALLBACK cmd_gun(std::string param)
{
	if (param.empty())
	{
		actor_info *actor = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE);
		byte slot = actor->weapon_slot;
		int weapID = actor->weapon[slot].id;
		actor->weapon[slot].ammo = 0;
		actor->weapon[slot].ammo_clip = 0;
		GAME->GetPools()->GetPedFromRef(1)->ClearWeapon((eWeaponType)weapID);
		notif->AddNotification(-1, "����� � ����� �������");
	}
	else
	{
		int weaponID, ammo = 0;
		sscanf(param.c_str(), "%d %d", &weaponID, &ammo);
		if (ammo == 0) ammo = 250;

		if ((weaponID >= 0 && weaponID < 19) || (weaponID > 21 && weaponID <= 46))
		{
			GAME->GetPools()->GetPedFromRef(1)->GiveWeapon((eWeaponType)weaponID, ammo, (eWeaponSkill)weap.byteSkill(ACTOR_SELF));
			notif->AddNotification(-1, "������ ������ %s(%d) � %d ���������", weap.get_name_by_id(weaponID).c_str(), weaponID, ammo);
		}
		else notif->AddNotification(0xFFFFCC40, "�� ������ ID ������");
	}
}

void CALLBACK cmd_warp(std::string param)
{
	if (param.empty())
	{
		notif->AddNotification(0xFFFFCC40, "���������� ������� ID ������");
		return;
	}

	int ID = atoi(param.c_str());
	if (SF->getSAMP()->getPlayers()->IsPlayerDefined(ID))
	{
		CVector vect = player.GetBonePosition(ID);
		if (vect.fX != 0.0 || vect.fY != 0.0)
			player.SetPosition(vect);
		else notif->AddNotification(0xFFFFCC40, "�� ������� ���������� ������� ������");
	}
	else notif->AddNotification(0xFFFFCC40, "����� � ����� ID �� ������");
}

void CALLBACK cmd_gp(std::string param)
{
	const std::regex GETPICKUP( R"(\s*(\d+)\s*(\w*)\s*)" );
	std::smatch match;
	if ( !std::regex_match( param, match, GETPICKUP ) )
	{
		notif->AddNotification( 0xFFFFCC40, "�� ������ ������ �������" );
		return;
	}
	int ID = atoi(match[1].str().c_str());
	std::string cmd = "/" + match[2].str();

	if (!SF->getSAMP()->getInfo()->pPools->pPickup->pickup[ID].iModelID)
	{
		notif->AddNotification(0xFFFFCC40, "��������� ����� �� ������");
		return;
	}
	if (!SF->getSAMP()->getInfo()->pPools->pPickup->pickup[ID].iType)
	{
		notif->AddNotification(0xFFFFCC40, "��������� ����� �� ������");
		return;
	}
	if (!SF->getSAMP()->getInfo()->pPools->pPickup->IsPickupExists(ID))
	{
		notif->AddNotification(0xFFFFCC40, "��������� ����� �� ������");
		return;
	}

	stOnFootData sync;
	memset(&sync, 0, sizeof(stOnFootData));
	sync = SF->getSAMP()->getPlayers()->pLocalPlayer->onFootData;
	*(CVector*)sync.fPosition = *(CVector*)SF->getSAMP()->getInfo()->pPools->pPickup->pickup[ID].fPosition;
	sync.sKeys = 0x0400;
	BitStream bsActorSync;
	bsActorSync.Write((BYTE)ID_PLAYER_SYNC);
	bsActorSync.Write((PCHAR)&sync, sizeof(stOnFootData));
	SF->getRakNet()->SendPacket(&bsActorSync);

	BitStream bsClass;
	bsClass.Write(uint32_t(ID));
	SF->getRakNet()->SendRPC(131, &bsClass);
	notif->AddNotification(-1, "����� %d ��������", ID);

	if ( !cmd.empty() )
		SF->getSAMP()->getPlayers()->pLocalPlayer->Say( (char*)cmd.c_str() );
}

void CALLBACK cmd_desCar( std::string param )
{
	int ID = atoi( param.c_str() );

	if ( !player.playerIsSpawned() ){
		notif->AddNotification( 0xFFFFCC40, "�� �� �����������?!" );
		return;
	}
	else if ( ID <= 0 || ID > 2000 ){
		notif->AddNotification( 0xFFFFCC40, "�� ������ ID ������!" );
		return;
	}
	else if ( SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID] == nullptr ){
		notif->AddNotification( 0xFFFFCC40, "��� ������ �� ����������" );
		return;
	}
	else if ( SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID]->pGTA_Vehicle == nullptr ){
		notif->AddNotification( 0xFFFFCC40, "��� ������ �� ����������" );
		return;
	}
	else if ( SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID]->pGTA_Vehicle->door_status == 2 ){
		notif->AddNotification( 0xFFFFCC40, "��� ������ �������" );
		return;
	}

	CVector *POS = GAME->GetPools()->GetPedFromRef( 1 )->GetPosition();
	SF->getSAMP()->sendEnterVehicle( ID, 0 );
	SF->getCLEO()->callOpcode( "072A: $PLAYER_ACTOR %d", SF->getSAMP()->getVehicles()->GetCarHandleFromSAMPCarID( ID ) );
	SF->getSAMP()->sendDamageVehicle( ID, rand() * 0x7FFFFFFF, rand() * 0x7FFFFFFF, rand() * 0x7F, rand() * 0x7F );
	SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID]->pGTA_Vehicle->hitpoints = 235.0f;
	stInCarData data = { 0 };
	*(CVector*)data.fMoveSpeed = CVector( 0.0, 0.0, 0.0 );
	if ( SF->getSAMP()->getVehicles()->pGTA_Vehicle[ID] != 0 ){
		int posOffset = (*(int*)(((int)SF->getSAMP()->getVehicles()->pGTA_Vehicle[ID]) + 20)) + 48;
		*(CVector*)data.fPosition = *(CVector*)posOffset;
		notif->AddNotification( 0xFFFFCC40, "������������ ������� ����" );
	}
	else {
		notif->AddNotification( 0xFFFFCC40, "������������ ���� �������" );
		*(CVector*)data.fPosition = *POS;
	}
	data.byteArmor = GAME->GetPools()->GetPedFromRef( 1 )->GetArmor();
	data.bytePlayerHealth = GAME->GetPools()->GetPedFromRef( 1 )->GetHealth();
	data.fVehicleHealth = 230.0f;
	data.sVehicleID = ID;

	BitStream carsync;
	carsync.Write( (BYTE)ID_VEHICLE_SYNC ); // ���������� ID ������.
	carsync.Write( (PCHAR)&data, sizeof( stInCarData ) ); // ���������� ������ �� ��������� sync
	SF->getRakNet()->SendPacket( &carsync ); // ���������� ����� �� ������.

	SF->getSAMP()->sendExitVehicle( ID );
	SF->getCLEO()->callOpcode( "0362: $PLAYER_ACTOR %f %f %f",
							   POS->fX, POS->fY, POS->fZ );
	SF->getSAMP()->sendVehicleDestroyed( ID );
}