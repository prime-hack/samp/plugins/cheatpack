// Is the BP/EP/FP/DP (special flags) status of the player as follows
#define PedSoft 0x01
#define FreezePed 0x02
#define BulletProtect 0x04
#define FireProtect 0x08
#define CollisionProtect 0x10
#define MeleeProtect 0x20
#define Unc_7bit 0x40
#define ExplosionProtect 0x80

#define AnimationFall 1208
#define AnimationFall2 1129

void MainCharacterEvents();
void AplyGodMode();
void AplyAcceleration();
void AplyAntiFall();
void BunnyHop(stOnFootData &data);
void DMZZ(stOnFootData &data);
void _stdcall RestoreHealth(int key);
void _stdcall RestoreArmor(int key);
void _stdcall FixCar(int key);
void BugC();