#include "main.h"

void mCharacterInit(D3DCOLOR &color)
{
	MENU.Character = new Menu("�����", -1, -1, 265, 150, color, -1, false);
	MENU.Character->SetParent(MENU.gl);
	PlayerGM_Init(color);
	CarGM_Init(color);
	MENU.Character->Tumblers->AddTumbler("�����.GM", 5, 5, MENU.submenu.PlayerGM->GetPresentPointer());
	MENU.Character->Tumblers->AddTumbler("������.GM", 85, 5, MENU.submenu.CarGM->GetPresentPointer());
	MENU.Character->Tumblers->AddTumbler("��������", 190, 5, &ACC.bunnyhop);
	MENU.Character->Tumblers->AddTumbler("����������", 5, 20, &ACC.GodMode);
	MENU.Character->Tumblers->AddTumbler("���������", 110, 20, &ACC.Acceleration);
	MENU.Character->Tumblers->AddTumbler("��� +C", 200, 20, &ACC.Bug_C);
	MENU.Character->Elements->AddElement("������������ ��", 5, 35, RestoreHealth);
	MENU.Character->Elements->AddElement("������������ ��", 5, 50, RestoreArmor);
	MENU.Character->Elements->AddElement("�������� ������", 5, 65, FixCar);
	MENU.Character->Tumblers->AddTumbler("�����������", 150, 35, &ACC.AntiFall);
	MENU.Character->Tumblers->AddTumbler("DM � ��", 150, 50, &ACC.DMZZ);

	stKeyThumblerEvent KEYEVENT;
	KEYEVENT.name = "����������";
	KEYEVENT.key = keyb.GetKeyNum(ini->esgString("�����", "�������.GM", "insert"));
	KEYEVENT.bVar = &ACC.GodMode;
	ThumblerEvent.push_back(KEYEVENT);
	KEYEVENT.name = "��� +C";
	KEYEVENT.key = keyb.GetKeyNum(ini->esgString("�����", "�������.��� +C", "9"));
	KEYEVENT.bVar = &ACC.Bug_C;
	ThumblerEvent.push_back(KEYEVENT);

	stKeyMenuCallbackEvent KEYMCEVENT;
	KEYMCEVENT.key = keyb.GetKeyNum(ini->esgString("�����", "�������.��", "&0"));
	KEYMCEVENT.pFunc = RestoreHealth;
	MenuCallbackEvent.push_back(KEYMCEVENT);
	KEYMCEVENT.key = keyb.GetKeyNum(ini->esgString("�����", "�������.��", "&0"));
	KEYMCEVENT.pFunc = RestoreArmor;
	MenuCallbackEvent.push_back(KEYMCEVENT);
	KEYMCEVENT.key = keyb.GetKeyNum(ini->esgString("�����", "�������.������", "1"));
	KEYMCEVENT.pFunc = FixCar;
	MenuCallbackEvent.push_back(KEYMCEVENT);

	ACC.GodMode = ini->esgBoolean("�����", "����������", false, true);
	ACC.bunnyhop = ini->esgBoolean("�����", "��������", false, true);
	ACC.Acceleration = ini->esgBoolean("�����", "���������", false, true);
	ACC.Bug_C = ini->esgBoolean("�����", "��� +C", false, true);
	ACC.AntiFall = ini->esgBoolean("�����", "�����������", false, true);
	ACC.DMZZ = ini->esgBoolean("�����", "DM � ��", false, true);
}

void PlayerGM_Init(D3DCOLOR &color)
{
	MENU.submenu.PlayerGM = new Menu("�����.GM", -1, -1, 130, 105, color, -1, false);
	MENU.submenu.PlayerGM->SetParent(MENU.gl);
	MENU.submenu.PlayerGM->Tumblers->AddTumbler("����", 5, 5, &GM.Bullet);
	MENU.submenu.PlayerGM->Tumblers->AddTumbler("�����", 5, 20, &GM.Fire);
	MENU.submenu.PlayerGM->Tumblers->AddTumbler("������������", 5, 35, &GM.Collision);
	MENU.submenu.PlayerGM->Tumblers->AddTumbler("�����", 5, 50, &GM.Melee);
	MENU.submenu.PlayerGM->Tumblers->AddTumbler("������", 5, 65, &GM.Explosion);

	GM.Bullet = ini->esgBoolean("�����.GM", "����", true, true);
	GM.Fire = ini->esgBoolean("�����.GM", "�����", false, true);
	GM.Collision = ini->esgBoolean("�����.GM", "������������", true, true);
	GM.Melee = ini->esgBoolean("�����.GM", "�����", false, true);
	GM.Explosion = ini->esgBoolean("�����.GM", "������", false, true);
}

void CarGM_Init(D3DCOLOR &color)
{
	MENU.submenu.CarGM = new Menu("������.GM", -1, -1, 130, 105, color, -1, false);
	MENU.submenu.CarGM->SetParent(MENU.gl);
	MENU.submenu.CarGM->Tumblers->AddTumbler("����", 5, 5, &GM.cBullet);
	MENU.submenu.CarGM->Tumblers->AddTumbler("�����", 5, 20, &GM.cFire);
	MENU.submenu.CarGM->Tumblers->AddTumbler("������������", 5, 35, &GM.cCollision);
	MENU.submenu.CarGM->Tumblers->AddTumbler("�����", 5, 50, &GM.cMelee);
	MENU.submenu.CarGM->Tumblers->AddTumbler("������", 5, 65, &GM.cExplosion);

	GM.cBullet = ini->esgBoolean("������.GM", "����", false, true);
	GM.cFire = ini->esgBoolean("������.GM", "�����", false, true);
	GM.cCollision = ini->esgBoolean("������.GM", "������������", true, true);
	GM.cMelee = ini->esgBoolean("������.GM", "�����", false, true);
	GM.cExplosion = ini->esgBoolean("������.GM", "������", false, true);
}

void mCharacterDestruct(bool SaveOnly)
{
	if (!SaveOnly)
	{
		delete MENU.submenu.PlayerGM;
		delete MENU.submenu.CarGM;
		delete MENU.Character;
	}
	ini->setBoolean("�����", "����������", ACC.GodMode, true);
	ini->setBoolean("�����", "��������", ACC.bunnyhop, true);
	ini->setBoolean("�����", "���������", ACC.Acceleration, true);
	ini->setBoolean("�����", "��� +C", ACC.Bug_C, true);
	ini->setBoolean("�����", "�����������", ACC.AntiFall, true);
	ini->setBoolean("�����", "DM � ��", ACC.DMZZ, true);

	ini->setBoolean("�����.GM", "����", GM.Bullet, true);
	ini->setBoolean("�����.GM", "�����", GM.Fire, true);
	ini->setBoolean("�����.GM", "������������", GM.Collision, true);
	ini->setBoolean("�����.GM", "�����", GM.Melee, true);
	ini->setBoolean("�����.GM", "������", GM.Explosion, true);

	ini->setBoolean("������.GM", "����", GM.cBullet, true);
	ini->setBoolean("������.GM", "�����", GM.cFire, true);
	ini->setBoolean("������.GM", "������������", GM.cCollision, true);
	ini->setBoolean("������.GM", "�����", GM.cMelee, true);
	ini->setBoolean("������.GM", "������", GM.cExplosion, true);
}