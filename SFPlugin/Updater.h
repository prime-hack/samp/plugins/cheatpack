#include <Wininet.h>
#pragma comment(lib, "Wininet")

class Updater
{
public:
	Updater(std::string URL, DWORD dwTime);
	bool CheckUpdate();
	void AutoChecker();
private:
	DWORD dwCheckTime = 60000;
	std::string URL;
	bool bSyncThread;
	static void NetThread(Updater*th);
};

extern Updater *upd;

