#include "main.h"

void AdminInit()
{
	AdminCheckerInfo.pos.x = *(int*)0xC9C040 * (ini->esgFloat("����������", "X", 0.5) / 100.0f);
	AdminCheckerInfo.pos.y = *(int*)0xC9C044 * (ini->esgFloat("����������", "Y", 66.666666) / 100.0f);
	AdminCheckerInfo.OutStream = ini->esgHex("����������", "��� ������", -1);
	AdminCheckerInfo.InStream = ini->esgHex("����������", "� ������", 0xFFFFCC40);

	IniFile *AC = new IniFile("SAMPFUNCS\\CheatPack\\AdminChecker.ini", "", false);

	char ServAddr[256];
	sprintf(ServAddr, "%s:%d\x00", SF->getSAMP()->getInfo()->szIP, SF->getSAMP()->getInfo()->ulPort);

	if (!AC->existKey("SERVERS", ServAddr))
	{
		delete AC;
		return;
	}

	std::string section = AC->getString("SERVERS", ServAddr);
	std::vector<std::string> srv = AC->getArrayString(section, "Admin");


	for (int i = 0; i < srv.size(); i++)
	{

		AdminCheckerInfo.admins_all.push_front(srv[i]);
		stConnectedPlayer connected;
		connected.ID = player.GetPlayerID(srv[i]);
		if (connected.ID >= 0 && connected.ID != SF->getSAMP()->getPlayers()->sLocalPlayerID)
		{
			connected.name = srv[i];
			AdminCheckerInfo.admins_connect.push_front(connected);
		}
	}
	//AC->AddArrayText("PH", "Admin", "Test");
	delete AC;
}

void SortAdminList(stRakNetHookParams *param)
{
	if (param->packetId == ScriptRPCEnumeration::RPC_ScrServerQuit)
	{
		short int playerId;
		param->bitStream->ResetReadPointer();
		param->bitStream->Read(playerId); // Id ������

		for (auto itr = AdminCheckerInfo.admins_connect.begin(); itr != AdminCheckerInfo.admins_connect.end(); ++itr)
			if (itr->ID == playerId)
			{
				AdminCheckerInfo.admins_connect.erase(itr);
				break;
			}
	}
	if (param->packetId == ScriptRPCEnumeration::RPC_ScrServerJoin) // ���� ��� RPC_ScrServerJoin
	{
		short int sPlayerID;
		D3DCOLOR D3DPlayerColor;
		byte isNPC, nameLen;
		char szPlayerName[25];

		param->bitStream->ResetReadPointer(); // �������� ������ ������.
		param->bitStream->Read(sPlayerID); // ������ ID ������.
		param->bitStream->Read(D3DPlayerColor); // ���� ���� ������.
		param->bitStream->Read(isNPC); // ���� ��������� � ���, NPC ��� ��� ���.
		param->bitStream->Read(nameLen); // ����� ����.
		param->bitStream->Read(szPlayerName, nameLen); // ���.
		szPlayerName[nameLen] = '\0'; // �������� ���� �� ���� ������
		param->bitStream->ResetReadPointer(); // �������� ������ ������

		for (auto itr = AdminCheckerInfo.admins_all.begin(); itr != AdminCheckerInfo.admins_all.end(); ++itr)
			if (!strcmp(szPlayerName, itr->c_str()))
			{
				if ( other.AdminReconnect ){
					Reconnect( 1 );
					break;
				}
				stConnectedPlayer connected;
				connected.ID = sPlayerID;
				connected.name = szPlayerName;
				AdminCheckerInfo.admins_connect.push_front(connected);
				break;
			}
	};
}

void RenderAdmins()
{
	int i = 0;
	D3DCOLOR color = AdminCheckerInfo.OutStream;
	for (auto itr = AdminCheckerInfo.admins_connect.begin(); itr != AdminCheckerInfo.admins_connect.end(); ++itr, ++i)
	{
		if (player.ActorDefined(itr->ID))
			color = AdminCheckerInfo.InStream;
		pFont->PrintShadow(itr->name.c_str(), color, AdminCheckerInfo.pos.x, AdminCheckerInfo.pos.y + i*pFont->DrawHeight());
	}
}