#include "main.h"

const struct weapon_entry	weapon_list[] =
{
	{ 0, 0, -1, "Fist" },
	{ 1, 0, 331, "Brass Knuckles" },
	{ 2, 1, 333, "Golf Club" },
	{ 3, 1, 334, "Nitestick" },
	{ 4, 1, 335, "Knife" },
	{ 5, 1, 336, "Baseball Bat" },
	{ 6, 1, 337, "Shovel" },
	{ 7, 1, 338, "Pool Cue" },
	{ 8, 1, 339, "Katana" },
	{ 9, 1, 341, "Chainsaw" },
	{ 22, 2, 346, "Pistol" },
	{ 23, 2, 347, "Silenced Pistol" },
	{ 24, 2, 348, "Desert Eagle" },
	{ 25, 3, 349, "Shotgun" },
	{ 26, 3, 350, "Sawn-Off Shotgun" },
	{ 27, 3, 351, "SPAZ12" },
	{ 28, 4, 352, "Micro UZI" },
	{ 29, 4, 353, "MP5" },
	{ 32, 4, 372, "Tech9" },
	{ 30, 5, 355, "AK47" },
	{ 31, 5, 356, "M4" },
	{ 33, 6, 357, "Country Rifle" },
	{ 34, 6, 358, "Sniper Rifle" },
	{ 35, 7, 359, "Rocket Launcher" },
	{ 36, 7, 360, "Heat Seeking RPG" },
	{ 37, 7, 361, "Flame Thrower" },
	{ 38, 7, 362, "Minigun" },
	{ 16, 8, 342, "Grenade" },
	{ 17, 8, 343, "Teargas" },
	{ 18, 8, 344, "Molotov Cocktail" },
	{ 39, 8, 363, "Remote Explosives" },
	{ 41, 9, 365, "Spray Can" },
	{ 42, 9, 366, "Fire Extinguisher" },
	{ 43, 9, 367, "Camera" },
	{ 10, 10, 321, "Dildo 1" },
	{ 11, 10, 322, "Dildo 2" },
	{ 12, 10, 323, "Vibe 1" },
	{ 13, 10, 324, "Vibe 2" },
	{ 14, 10, 325, "Flowers" },
	{ 15, 10, 326, "Cane" },
	{ 44, 11, 368, "NV Goggles" },
	{ 45, 11, 369, "IR Goggles" },
	{ 46, 11, 371, "Parachute" },
	{ 40, 12, 364, "Detonator" },
	{ -1, -1, -1, NULL }
};

float WeaponFunc::getDamage(int weaponId, byte skill)
{
	if (weaponId < 22 || weaponId > 32) skill = 0;
	uint16_t dmg = weaponId;
	if (skill == 1)
		dmg += 25;
	if (skill == 2)
		dmg += 36;
	dmg = *(uint16_t*)((dmg * 112) + 0xC8AADA);
	if ((weaponId < 23) || (weaponId > 38))
		dmg = 9.0f;
	if ((weaponId == 17) || (weaponId == 40) || (weaponId == 43) || (weaponId == 44) || (weaponId == 45))
		dmg = 0.0f;
	if ((weaponId != 25) && (weaponId != 26) && (weaponId != 27) && (weaponId != 16))
		dmg *= 0.33000001;
	return dmg;
};

float WeaponFunc::getSkill(int weaponId)
{
	float Skill = 10.0f;
	if ((weaponId >= 22) && (weaponId <= 32))
	{
		if (weaponId == 32)
			weaponId -= 4;
		Skill = *(float*)((weaponId - 22) * 4 + 0xB79494);
	}
	return Skill / 10.0f;
};

byte WeaponFunc::byteSkill(int PlayerID)
{
	if (PlayerID == SF->getSAMP()->getPlayers()->sLocalPlayerID || PlayerID == ACTOR_SELF)
		return 2; //SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->weapon_skill;
	else if (SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor != 0)
		return 2; //SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor->pGTA_Ped->weapon_skill;
	return 0;
};

void WeaponFunc::setSkill(int weaponId, float Skill)
{
	if ((weaponId >= 22) && (weaponId <= 32))
	{
		if (weaponId == 32)
			weaponId -= 4;
		*(float*)((weaponId - 22) * 4 + 0xB79494) = Skill * 10.0f;
	}
};

void WeaponFunc::Distance(int weaponID, byte skill, float *Target, float *Bullet)
{
	if (weaponID < 22 || weaponID > 32) skill = 0;
	if (skill == 1)
		weaponID += 25;
	if (skill == 2)
		weaponID += 36;
	weaponID = (weaponID * 112) + 0xC8AAB8;
	if (Target != NULL) //NULL ������ �� ��������, �������� ���� ��� ���� ������� ������ 1 �� ��������� ������� ������ �� ��������
		Target = (float*)(weaponID + 0x4); //��������� �� ������� ����� ������� �����������
	if (Bullet != NULL)
		Bullet = (float*)(weaponID + 0x8); //��������� �� ������� ���������� ����
};

float* WeaponFunc::Recoil(int weaponID, byte skill)
{
	if (weaponID < 22 || weaponID > 32) skill = 0;
	if (skill == 1)
		weaponID += 25;
	if (skill == 2)
		weaponID += 36;
	weaponID = (weaponID * 112) + 13150904; //+0xC8AAF0
	return (float*)(weaponID + 0x38);
};

bool WeaponFunc::UnlimAmmo(byte state)
{
	if (state != -1)
		*(BYTE*)0x969178 = state;
	return *(BYTE*)0x969178;
};

float WeaponFunc::getSpeed(int weap)
{
	float Speed = 1.5;
	if (weap >= 22 && weap <= 38)
	{
		Speed = (1000.0 / 125.0);
		if (weap == 23 || weap == 24)
			Speed = (1000.0 / 750.0);
		if (weap == 25 || weap == 33 || weap == 34)
			Speed = (1000.0 / 825.0);
		if (weap == 26 || weap == 27 || weap == 22)
			Speed = (1000.0 / 250.0);
		if (weap == 38 || weap == 37)
			Speed = (1000.0 / 40.0);
		if (weap == 28 || weap == 32)
			Speed = (1000.0 / 90.0);
	}
	return Speed * 42.75;
}

WORD* WeaponFunc::AmmoClip(int weaponID, byte skill)
{
	if (weaponID < 22 || weaponID > 32) skill = 0;
	if (skill == 1)
		weaponID += 25;
	if (skill == 2)
		weaponID += 36;
	weaponID = (weaponID * 112) + 0xC8AAB8;
	return (WORD*)(weaponID + 0x20);
};

std::string WeaponFunc::get_name_by_id(int userWeaponID)
{
	for (UINT i = 0; i < 45; i++)
		if (weapon_list[i].id == userWeaponID)
			return weapon_list[i].name;

	return "Error";
};

void WeaponFunc::loadAllWeaponModels(void)
{
	static int	models_loaded;
	if (!models_loaded)
	{
		const struct weapon_entry	*weapon;
		for (weapon = weapon_list; weapon->name != NULL; weapon++)
		{
			if (weapon->model_id == -1)
				continue;
			if (GTAfunc_isModelLoaded(weapon->model_id))
				continue;
			GTAfunc_requestModelLoad(weapon->model_id);
			GTAfunc_loadRequestedModels();
		}

		models_loaded = 1;
	}
}