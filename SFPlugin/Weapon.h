struct weapon_entry
{
	int			id;
	int			slot;
	int			model_id;
	const char	*name;
};

class WeaponFunc
{
public:
	float getDamage(int weaponId, byte skill);
	float getSkill(int weaponId);
	byte byteSkill(int PlayerID);
	void setSkill(int weaponId, float Skill);
	void Distance(int weaponID, byte skill, float *Target, float *Bullet = NULL);
	float* Recoil(int weaponID, byte skill);
	bool UnlimAmmo(byte state);
	float getSpeed(int weap);
	WORD* AmmoClip(int weaponID, byte skill);
	std::string get_name_by_id(int userWeaponID);
	void loadAllWeaponModels(void);
};
extern WeaponFunc weap;