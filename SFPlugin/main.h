//#define _CP_version "0.8.5"
extern char* szVersion;

// illegal instruction size
#pragma warning( disable : 4409 )

#include <windows.h>
#include <string>
#include <fstream>
#include <assert.h>
#include <process.h>
#include <Winbase.h>
#include <Shlobj.h>
#include <regex>
#include <thread>
#include <atomic>
#include <future>
#pragma comment(lib, "Advapi32")
#pragma comment(lib, "Shell32")
#pragma comment(lib, "WS2_32")

#include "SAMPFUNCS_API.h"
#include "game_api\game_api.h"

template<typename T> void VectorErase( std::vector<T> &vec, T v );

extern SAMPFUNCS *SF;
#include "!MenuManager.h"
#include "Keys.h"
#include "IniFile.h"
#include "Player.h"
#include "World.h"
#include "Weapon.h"
#include "BotBase.h"
#include "RegMenu.h"
#include "NetWork.h"
#include "Teleports.h"
#include "Proc.h"
#include "Commands.h"
#include "MainCharacter.h"
#include "Notification.h"
//#include "Queue.h"
#include "Binder.h"
#include "AdminChecker.h"
#include "Updater.h"

extern stFontInfo *pFont;
bool CALLBACK Present(CONST RECT *pSourceRect, CONST RECT *pDestRect, HWND hDestWindowOverride, CONST RGNDATA *pDirtyRegion);
bool CALLBACK WindowEvents(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
