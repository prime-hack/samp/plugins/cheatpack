#include "main.h"

Notification::Notification(stFontInfo *pFont, POINT pos, byte LogMode, byte StringLimit)
{
	this->pFont = pFont;
	this->pos = pos;
	if (this->pos.x < 0)
		this->pos.x = 0;
	if (this->pos.y < 0)
		this->pos.y = 0;
	if (this->pos.x > *(int*)0xC9C040)
		this->pos.x = *(int*)0xC9C040;
	if (this->pos.y > *(int*)0xC9C044 - pFont->DrawHeight())
		this->pos.y = *(int*)0xC9C044 - pFont->DrawHeight();
	this->LogMode = LogMode;
	this->StringLimit = StringLimit;
}

bool Notification::AddNotification(D3DCOLOR color, const char* text, ...) //����� ������ ������� �� ����� ������
{
	if (!this->LogMode)
		return false;

	va_list ap;
	char *str = new char[strlen(text)*2];
	va_start(ap, text);
	vsprintf(str, text, ap);
	va_end(ap);

	stNotificationInfo NotInfo;
	NotInfo.color = color;
	NotInfo.dwTime = strlen(str) * 125 + 1500 + GetTickCount();
	NotInfo.text = str;

	this->Round.push_front(NotInfo);
	switch (this->LogMode)
	{
	case 3:
		SF->LogConsole(str); //��� ������ ��� ��� ���������� ������, � �� ���� ������� "����� ����" (2 �� 1), ��������� ��� �� :D
	case 2:
		SF->LogFile(str);
		break;
	default:
		break;
	}

	delete str;
	return true;
}

bool Notification::AddNotification(D3DCOLOR color, UINT wait, const char* text, ...) //����� ������ �����������
{
	if (!this->LogMode)
		return false;

	va_list ap;
	char *str = new char[strlen(text)*2];
	va_start(ap, text);
	vsprintf(str, text, ap);
	va_end(ap);

	stNotificationInfo NotInfo;
	NotInfo.color = color;
	NotInfo.dwTime = wait + GetTickCount();
	NotInfo.text = str;

	this->Round.push_front(NotInfo);
	switch (this->LogMode)
	{
	case 3:
		SF->LogConsole(str); //��� ������ ��� ��� ���������� ������, � �� ���� ������� "����� ����" (2 �� 1), ��������� ��� �� :D
	case 2:
		SF->LogFile(str);
		break;
	default:
		break;
	}
	delete str;
	return true;
}

void Notification::RenderNotification()
{
	if (!this->Round.size())
		return;

	int i = 0;
	for (auto itr = this->Round.begin(); itr != this->Round.end(); ++itr, ++i)
	{
		if (i >= this->StringLimit)
			break;
		if ((int)(itr->dwTime - GetTickCount()) <= 0)
		{
			if (this->Round.size() == 1)
			{
				this->Round.pop_back();
				return;
			}
			itr-- = this->Round.erase(itr);
			//--itr; //����, ����� ������ "itr = this->Round.erase(itr)", �� ��� ������-�� ����� ������� � 1 ����� ��� �������� ������
			--i;
			continue;
		}
		D3DCOLOR color = itr->color;
		if (itr->dwTime - GetTickCount() < 0xFF)
		{
			byte ARGB[4];
			SF->getRender()->ARGB_To_A_R_G_B(color, ARGB[0], ARGB[1], ARGB[2], ARGB[3]);
			ARGB[0] = (itr->dwTime - GetTickCount());
			color = D3DCOLOR_ARGB(ARGB[0], ARGB[1], ARGB[2], ARGB[3]);
		}
		int X = pos.x;
		if (X > *(int*)0xC9C040 / 2 && X + this->pFont->DrawLength(itr->text.c_str()) > *(int*)0xC9C040)
			X = *(int*)0xC9C040 - this->pFont->DrawLength(itr->text.c_str());
		this->pFont->PrintShadow(itr->text.c_str(), color, X, pos.y + i*this->pFont->DrawHeight());
	}
}