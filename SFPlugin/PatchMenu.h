struct stMemPatch
{
	std::string name;
	bool status;
	DWORD addr;
	size_t len;
	byte *orig;
	byte *repl;
};

void PatchInit(D3DCOLOR &color);

mcall PatchGTAEvent(int Select, byte key);
mcall PatchSAMPEvent(int Select, byte key);

void PatchesDestruct();