#include "main.h"
std::vector<stMemPatch> GTA_patches, SAMP_patches;

void PatchInit(D3DCOLOR &color)
{
	MENU.patch = new Menu("�����", -1, -1, 500, 210, color, -1, false);
	MENU.patch->SetParent(MENU.gl);
	MENU.patch->Listings->AddListing(5, 5, 245, 180, PatchGTAEvent);
	MENU.patch->Listings->AddListing(255, 5, 245, 180, PatchSAMPEvent);

	IniFile *nop = new IniFile("SAMPFUNCS\\CheatPack\\Patches.ini", "", false);


	stMemPatch MemPatch;
	std::vector <std::string> patch = nop->getArrayString("PATCHES", "GTA");

	for (int i = 0; i < patch.size(); i++)
	{
		std::vector <std::string> params = nop->getParams(patch[i]);
		if (params.size() != 5) continue; // addr, value, name, autorun

		sscanf(params[0].c_str(), "%X", &MemPatch.addr); //Get address
		MemPatch.len = params[1].length() / 2; //Get lenght

		MemPatch.repl = new byte[MemPatch.len];
		Str2Bytes(params[1], MemPatch.repl); //Get value

		MemPatch.orig = new byte[MemPatch.len];

		MemPatch.name = params[2]; //Set name patch

		if (!strcmp("true", (char*)convToLowercase((char*)params[3].c_str()).c_str())
			|| !strcmp("1", (char*)convToLowercase((char*)params[3].c_str()).c_str()))
		{
			memcpy_safe(MemPatch.orig, (void*)MemPatch.addr, MemPatch.len); //Get original data
			memcpy_safe((void*)MemPatch.addr, MemPatch.repl, MemPatch.len);
			MENU.patch->Listings->AddTextOnListing(0, "{FF40FF40}%s", params[2].c_str());
			MemPatch.status = true;
		}
		else
		{
			MENU.patch->Listings->AddTextOnListing(0, params[2].c_str());
			MemPatch.status = false;
		}

		stKeyPatchesEvent KEYEVENT;
		KEYEVENT.name = MemPatch.name;
		KEYEVENT.key = keyb.GetKeyNum(params[4]);
		KEYEVENT.status = MemPatch.status;
		KEYEVENT.number = i;
		KEYEVENT.pFunc = PatchGTAEvent;
		PathesEvent.push_back(KEYEVENT);

		GTA_patches.push_back(MemPatch); //Save patch in vector
	}


	//SAMP
	patch.clear();
	patch.shrink_to_fit();
	patch = nop->getArrayString("PATCHES", "SAMP");

	for (int i = 0; i < patch.size(); i++)
	{
		std::vector <std::string> params = nop->getParams(patch[i]);
		if (params.size() != 5) continue; // addr, value, name, autorun

		sscanf(params[0].c_str(), "%X", &MemPatch.addr);
		MemPatch.addr += SF->getSAMP()->getSAMPAddr(); //Get address
		MemPatch.len = params[1].length() / 2; //Get lenght

		MemPatch.repl = new byte[MemPatch.len];
		Str2Bytes(params[1], MemPatch.repl); //Get value

		MemPatch.orig = new byte[MemPatch.len];

		MemPatch.name = params[2]; //Set name patch

		if (!strcmp("true", (char*)convToLowercase((char*)params[3].c_str()).c_str())
			|| !strcmp("1", (char*)convToLowercase((char*)params[3].c_str()).c_str()))
		{
			memcpy_safe(MemPatch.orig, (void*)MemPatch.addr, MemPatch.len); //Get original data
			memcpy_safe((void*)MemPatch.addr, MemPatch.repl, MemPatch.len);
			MENU.patch->Listings->AddTextOnListing(1, "{FF40FF40}%s", params[2].c_str());
			MemPatch.status = true;
		}
		else
		{
			MENU.patch->Listings->AddTextOnListing(1, params[2].c_str());
			MemPatch.status = false;
		}

		stKeyPatchesEvent KEYEVENT;
		KEYEVENT.name = MemPatch.name;
		KEYEVENT.key = keyb.GetKeyNum(params[4]);
		KEYEVENT.status = MemPatch.status;
		KEYEVENT.number = i;
		KEYEVENT.pFunc = PatchSAMPEvent;
		PathesEvent.push_back(KEYEVENT);

		SAMP_patches.push_back(MemPatch); //Save patch in vector
	}

	delete nop;
}

mcall PatchGTAEvent(int ID, byte key)
{
	if (!GTA_patches[ID].status)
	{
		memcpy_safe(GTA_patches[ID].orig, (void*)GTA_patches[ID].addr, GTA_patches[ID].len); //Get original data
		memcpy_safe((void*)GTA_patches[ID].addr, GTA_patches[ID].repl, GTA_patches[ID].len);
		MENU.patch->Listings->SetTextOnListingByID(0, ID, "{FF40FF40}%s", GTA_patches[ID].name.c_str());
	}
	else
	{
		memcpy_safe((void*)GTA_patches[ID].addr, GTA_patches[ID].orig, GTA_patches[ID].len);
		MENU.patch->Listings->SetTextOnListingByID(0, ID, GTA_patches[ID].name.c_str());
	}
	GTA_patches[ID].status ^= true;
}

mcall PatchSAMPEvent(int ID, byte key)
{
	if (!SAMP_patches[ID].status)
	{
		memcpy_safe(SAMP_patches[ID].orig, (void*)SAMP_patches[ID].addr, SAMP_patches[ID].len); //Get original data
		memcpy_safe((void*)SAMP_patches[ID].addr, SAMP_patches[ID].repl, SAMP_patches[ID].len);
		MENU.patch->Listings->SetTextOnListingByID(1, ID, "{FF40FF40}%s", SAMP_patches[ID].name.c_str());
	}
	else
	{
		memcpy_safe((void*)SAMP_patches[ID].addr, SAMP_patches[ID].orig, SAMP_patches[ID].len);
		MENU.patch->Listings->SetTextOnListingByID(1, ID, SAMP_patches[ID].name.c_str());
	}
	SAMP_patches[ID].status ^= true;
}

void PatchesDestruct()
{
	for (int i = 0; i < GTA_patches.size(); i++)
		if (GTA_patches[i].status)
			memcpy_safe((void*)GTA_patches[i].addr, GTA_patches[i].orig, GTA_patches[i].len);
	for (int i = 0; i < SAMP_patches.size(); i++)
		if (SAMP_patches[i].status)
			memcpy_safe((void*)SAMP_patches[i].addr, SAMP_patches[i].orig, SAMP_patches[i].len);

	delete MENU.patch;
}