#include "main.h"
std::string convToLowercase(char *str)
{
	int sz = strlen(str);
	char ch[1024];
	memset(ch, 0, sizeof(ch));

	strcpy(ch, str);
	for (int i = 0; i < sz; i++)
	{
		if (ch[i] >= 0x41 && ch[i] <= 0x5a) ch[i] += 0x20;
		else if (ch[i] >= 0xc0 && ch[i] <= 0xdf) ch[i] += 0x20;
	}

	return (const char*)ch;
}

void KeyEvent(UINT msg, WPARAM wParam)
{
	if (SF->isConsoleOpened())
		return;

	if (SF->getSAMP()->getInput()->iInputEnabled)
		return;

	DWORD MenuTopAddr = GetTopAddr();
	if (MenuTopAddr != NULL)
		if (IsInput(MenuTopAddr))
			return;

	if (msg == WM_KEYDOWN)
	{
		for (auto itr = PathesEvent.begin(); itr != PathesEvent.end(); ++itr)
		{
			if (wParam == itr->key)
			{
				DWORD pFunc = (DWORD)itr->pFunc;
				DWORD ID = itr->number;
				byte c = 0;
				__asm
				{
					push c
					push ID
					call pFunc
				}
				itr->status ^= true;
				notif->AddNotification(itr->status ? 0xFF40FF40 : 0xFFFF4040, itr->name.c_str());
			}
		}
		for (auto itr = ThumblerEvent.begin(); itr != ThumblerEvent.end(); ++itr)
		{
			if (wParam == itr->key)
			{
				*itr->bVar ^= true;
				notif->AddNotification(*itr->bVar ? 0xFF40FF40 : 0xFFFF4040, itr->name.c_str());
			}
		}
		for (auto itr = BindEvent.begin(); itr != BindEvent.end(); ++itr)
		{
			if (wParam == itr->key)
			{
				if (!itr->target)
					SF->getSAMP()->getPlayers()->pLocalPlayer->Say((char*)itr->message.c_str());
				else
				{
					if (player.Driving(ACTOR_SELF))
					{
						notif->AddNotification(0xFFFFCC40, "�� � ������!");
						continue;
					}

					if (!keyb.GetGameKeyState(6))
					{
						notif->AddNotification(0xFFFFCC40, "���������� �����������");
						continue;
					}

					int ID = player.SearshTargetPlayer();
					if (player.ActorDefined(ID))
					{
						char msg[256];
						sprintf(msg, (char*)itr->message.c_str(), ID);
						SF->getSAMP()->getPlayers()->pLocalPlayer->Say(msg);
					}
					else notif->AddNotification(0xFFFFCC40, "��� ������ ��� ��������");
				}
			}
		}
		for (auto itr = MenuCallbackEvent.begin(); itr != MenuCallbackEvent.end(); ++itr)
		{
			if (wParam == itr->key)
			{
				DWORD pFunc = (DWORD)itr->pFunc;
				int key = 0;
				__asm
				{
					push key
					call pFunc
				}
			}
		}
	}
}