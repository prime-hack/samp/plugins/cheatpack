#include "main.h"

void MainCharacterEvents()
{
	if ( SF->getGame()->isGTAMenuActive() )
		return;
	if (!player.playerIsSpawned())
		return;
	if ( player.ActorDead( -1 ) )
		return;

	AplyGodMode();
	AplyAcceleration();
	AplyAntiFall();
}

void AplyGodMode()
{
	if (ACC.GodMode)
	{
		SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->flags |=
			(GM.Bullet * BulletProtect + GM.Fire * FireProtect + GM.Collision * CollisionProtect +
			GM.Melee * MeleeProtect + GM.Explosion * ExplosionProtect);

		/*if (GM.Bullet && GM.Fire && GM.Collision && GM.Melee && GM.Explosion)
			SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->hitpoints = GM.fPlayerHealth;

		else GM.fPlayerHealth = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->hitpoints;*/
	}
	else
	{
		SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->flags &=
			~(BulletProtect + FireProtect + CollisionProtect + MeleeProtect + ExplosionProtect);

		//GM.fPlayerHealth = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->hitpoints;
	}

	//For car
	if (!player.isDriver(ACTOR_SELF))
	{
		if (player.LeavingCar(ACTOR_SELF))
			SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->flags = 0;
		return;
	}

	if (ACC.GodMode)
	{
		SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->flags |=
			(GM.cBullet * BulletProtect + GM.cFire * FireProtect + GM.cCollision * CollisionProtect +
			GM.cMelee * MeleeProtect + GM.cExplosion * ExplosionProtect);

		/*if (SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->vehicle != NULL)
		{
			if (GM.cBullet && GM.cFire && GM.cCollision && GM.cMelee && GM.cExplosion)
				SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->vehicle->hitpoints = GM.fVehicleHealth;
			else GM.fVehicleHealth = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->hitpoints;
		}*/
	}
	else
	{
		SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->flags &=
			~(BulletProtect + FireProtect + CollisionProtect + MeleeProtect + ExplosionProtect);

		/*if (SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->vehicle != NULL)
			GM.fVehicleHealth = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->vehicle->hitpoints;*/
	}
}

void AplyAcceleration()
{
	if (!ACC.Acceleration)
		return;

	if (player.isDriver(ACTOR_SELF))
	{
		SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->speed[0] *= 1.000360453125;
		SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->speed[1] *= 1.000360453125;
	}
	else
	{
		DWORD addr = *(DWORD*)((DWORD)SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped + 0x14);
		addr += 0x30; //PosX
		*(float*)addr += SF->getSAMP()->getPlayers()->pLocalPlayer->onFootData.fMoveSpeed[0] * 0.135;
		addr += 0x04; //Next
		*(float*)addr += SF->getSAMP()->getPlayers()->pLocalPlayer->onFootData.fMoveSpeed[1] * 0.135;
	}
}

void AplyAntiFall()
{
	if (!ACC.AntiFall)
		return;

	int AnimID = SF->getSAMP()->getPlayers()->pLocalPlayer->sCurrentAnimID;
	float HP = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->hitpoints;
	if ( (AnimID != AnimationFall && AnimID != AnimationFall2) || HP < 1.0f )
		return;

	CVector *POS = GAME->GetPools()->GetPedFromRef(1)->GetPosition();
	GAME->GetPools()->GetPedFromRef(1)->Teleport(POS->fX, POS->fY, POS->fZ);
}

void BunnyHop(stOnFootData &data)
{
	if (!ACC.bunnyhop) return;

	if (data.sKeys == 40)
		data.sKeys = 32;
	if (data.sCurrentAnimationID == 1198)
		data.sCurrentAnimationID = 1195;
}

void DMZZ(stOnFootData &data)
{
	if (!ACC.DMZZ) return;

	if ( data.stSampKeys.keys_primaryFire || data.stSampKeys.keys_secondaryFire__shoot )
		data.sAnimFlags = 32776;
	else data.sAnimFlags = 33000;
	if ( (data.sCurrentAnimationID >= 1136 && data.sCurrentAnimationID <= 1138) ||
		 data.sCurrentAnimationID == 1141 || data.sCurrentAnimationID == 504 || data.sCurrentAnimationID == 505 )
		data.sCurrentAnimationID = 1135;
}

void _stdcall RestoreHealth(int key)
{
	SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->hitpoints = *(float*)0xB793E0 / 5.69;
	notif->AddNotification(-1, "�������� ��������������!");
}

void _stdcall RestoreArmor(int key)
{
	SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->armor = 100.0f;
	notif->AddNotification(-1, "����� ��������������!");
}

void _stdcall FixCar(int key)
{
	if (!player.isDriver(ACTOR_SELF))
	{
		notif->AddNotification(0xFFFFCC40, "�� �� � ������!");
		return;
	}

	struct vehicle_info* car = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTAEntity->vehicle;
	if (car == NULL)
	{
		notif->AddNotification(0xFFFFCC40, "�� ������� �������� ��������� ������");
		return;
	}

	CVehicle *veh = GAME->GetPools()->GetVehicle((DWORD*)car);
	if (veh == NULL)
	{
		notif->AddNotification(0xFFFFCC40, "�� ������� �������� ����� ������");
		return;
	}

	car->hitpoints = 1000.0f;
	SF->Log("pointer to current car hitpoints: %08X", &car->hitpoints);
	veh->Fix();
	notif->AddNotification(-1, "������ ���������������!");
}

void BugC()
{
	if (!ACC.Bug_C) return;

	byte mWP = SF->getSAMP()->getPlayers()->pLocalPlayer->byteCurrentWeapon;
	if (mWP != 24 && mWP != 25 && mWP != 23 && mWP != 33 && mWP != 34)
		return;

	stOnFootData sync; // ��������� ������ ��������� stOnFootData, � ������� �������� ������.
	memset(&sync, 0, sizeof(stOnFootData)); // ��������.
	sync = SF->getSAMP()->getPlayers()->pLocalPlayer->onFootData; // �������� ������ �� ��������� ���������� ������.
	sync.sKeys = 2;
	sync.stSampKeys.keys_primaryFire = 0;
	sync.stSampKeys.keys_aim = 0;
	sync.stSampKeys.keys_horn__crouch = 1;
	sync.sCurrentAnimationID = 1179; // ������ ������ ��������
	BitStream bsActorSync; // ��������� ������ ������ BitStream, � ������� �������� �������� ������. 
	bsActorSync.Write((BYTE)ID_PLAYER_SYNC); // ���������� ID ������.
	bsActorSync.Write((PCHAR)&sync, sizeof(stOnFootData)); // ���������� ������ �� ��������� sync
	SF->getRakNet()->SendPacket(&bsActorSync); // ���������� ����� �� ������.

	CVector *POS = GAME->GetPools()->GetPedFromRef(1)->GetPosition();
	GAME->GetPools()->GetPedFromRef(1)->Teleport(POS->fX, POS->fY, POS->fZ);
}