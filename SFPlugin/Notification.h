struct stNotificationInfo
{
	DWORD dwTime; //= len * 125 + 1500
	std::string text;
	D3DCOLOR color; //alpha = 255ms time left
};

class Notification
{
	stFontInfo *pFont;
	POINT pos;
	byte LogMode; //0 - no log, 1 - screen, 2 - screen + file, 3 - full
	byte StringLimit; //������������ �����
	std::list<stNotificationInfo> Round; //��������� �������
public:
	Notification(stFontInfo *pFont, POINT pos, byte LogMode = 2, byte StringLimit = 10);
	bool AddNotification(D3DCOLOR color, const char* text, ...); //����� ������ ������� �� ����� ������
	bool AddNotification(D3DCOLOR color, UINT wait, const char* text, ...);  //����� ������ �����������
	void RenderNotification();
};

extern Notification *notif;

