#define HOOKPOS_PlayerCollision 0x0054BCEE

void CollisInit(D3DCOLOR &color);
void CollisDestruct(bool SaveOnly = true);
bool CollisionCheck(object_base *obj1, object_base *obj2);
struct stCollision
{
	bool Players;
	bool Cars;
	bool Objects;
	bool Buildings;
	
	byte patch[6];
};