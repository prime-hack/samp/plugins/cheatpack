#define SAMP_COLOR_OFFSET							0x216378
class PlayerFunc
{
public:
	bool Driving(int PlayerID);
	bool isDriver(int PlayerID);
	bool LeavingCar(int PlayerID);
	bool ActorDefined(int ID);
	bool ActorDead(int ID);
	bool IsLocal(int ID);
	bool playerIsSpawned();
	CVector GetBonePosition(int PlayerID, byte BoneID = 1);
	bool inCar(int PlayerID, int CarID);
	int GetCarID(int PlayerID);
	void SetPosition(CVector vect);
	D3DCOLOR GetColorRGBA(int id);
	void SetColorRGBA(int id, D3DCOLOR RGBA);
	int SearshTargetPlayer();
	int GetPlayerID(std::string nick);
	void SetQuaternion(CVector4D quaternion);
	CVector4D GetQuaternion(int PlayerID);
private:
	void SetMatrix(VECTOR &right, VECTOR &up, VECTOR &at);
	void QuaternionToMatrix(float &quaterW, float &quaterX, float &quaterY, float &quaterZ, VECTOR* right, VECTOR* up, VECTOR* at);
	void PlayerFunc::MatrixToQuaternion(VECTOR &right, VECTOR &up, VECTOR &at, float *quaterW, float *quaterX, float *quaterY, float *quaterZ); // IDA POWA !
	void PlayerFunc::GetMatrix(int PlayerID, VECTOR &right, VECTOR &up, VECTOR &at);
};
extern PlayerFunc player;