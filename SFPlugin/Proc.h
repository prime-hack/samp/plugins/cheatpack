#define FUNC_RequestModel				0x4087e0
#define FUNC_LoadAllRequestedModels		0x40ea10

void ScreenFont();
struct stlTimer
{
	void *addr;
	UINT time;
	bool pass;
};
bool __stdcall lTimer(UINT time);
void *memcpy_safe(void *_dest, const void *_src, size_t stLen);
void Str2Bytes(std::string &str, byte *mas);
int GTAfunc_isModelLoaded(int iModelID);
void GTAfunc_requestModelLoad(int iModelID);
void GTAfunc_loadRequestedModels(void);