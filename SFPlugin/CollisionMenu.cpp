#include "main.h"
stCollision Collis;

void __declspec (naked) HOOK_PlayerCollision(void)
{
	static object_base *_obj1, *_obj2;
	static DWORD RETURN_ovrwr = 0x54CEFC, RETURN_process = 0x0054BCF4, RETURN_noProcessing = 0x54CF8D;
	__asm
	{
		jz hk_PlCol_process
			jmp RETURN_ovrwr
	}

hk_PlCol_process:
	__asm
	{
		pushad
			mov _obj2, esi // processing collision of *_info (esi)
			mov _obj1, edi // with *_info (edi)
	}

	// process collision if cheat not enabled
	//goto hk_PlCol_processCol;

	// already crashed, if true
	if (_obj1 == nullptr || _obj2 == nullptr)
		goto hk_PlCol_noCol;

	// check for disable collision
	if (CollisionCheck(_obj1, _obj2))
		goto hk_PlCol_noCol;

	if (Collis.Buildings)
	{
		__asm popad;
		__asm jmp RETURN_ovrwr
	}

hk_PlCol_processCol:
	__asm popad
	__asm jmp RETURN_process

hk_PlCol_noCol :
	__asm popad
	__asm jmp RETURN_noProcessing
}

void CollisInit(D3DCOLOR &color)
{
	MENU.collis = new Menu("��������", -1, -1, 85, 85, color, -1, false);
	MENU.collis->SetParent(MENU.gl);
	MENU.collis->Tumblers->AddTumbler("������", 5, 5, &Collis.Cars);
	MENU.collis->Tumblers->AddTumbler("������", 5, 20, &Collis.Players);
	MENU.collis->Tumblers->AddTumbler("�������", 5, 35, &Collis.Objects);
	MENU.collis->Tumblers->AddTumbler("���������", 5, 50, &Collis.Buildings);

	Collis.Cars = ini->esgBoolean("��������", "������", false, true);
	Collis.Players = ini->esgBoolean("��������", "������", false, true);
	Collis.Objects = ini->esgBoolean("��������", "�������", false, true);
	Collis.Buildings = ini->esgBoolean("��������", "���������", false, true);

	stKeyThumblerEvent KEYEVENT;
	KEYEVENT.name = "��������.������";
	KEYEVENT.key = keyb.GetKeyNum(ini->esgString("��������", "�������.������", "K"));
	KEYEVENT.bVar = &Collis.Cars;
	ThumblerEvent.push_back(KEYEVENT);
	KEYEVENT.name = "��������.������";
	KEYEVENT.key = keyb.GetKeyNum(ini->esgString("��������", "�������.������", "&0"));
	KEYEVENT.bVar = &Collis.Players;
	ThumblerEvent.push_back(KEYEVENT);
	KEYEVENT.name = "��������.�������";
	KEYEVENT.key = keyb.GetKeyNum(ini->esgString("��������", "�������.�������", "O"));
	KEYEVENT.bVar = &Collis.Objects;
	ThumblerEvent.push_back(KEYEVENT);
	KEYEVENT.name = "��������.���������";
	KEYEVENT.key = keyb.GetKeyNum(ini->esgString("��������", "�������.���������", "B"));
	KEYEVENT.bVar = &Collis.Buildings;
	ThumblerEvent.push_back(KEYEVENT);

	memcpy_safe(Collis.patch, (void *)HOOKPOS_PlayerCollision, 6);
	SF->getGame()->createHook((void *)HOOKPOS_PlayerCollision, HOOK_PlayerCollision, DETOUR_TYPE_JMP, 6);
}

void CollisDestruct(bool SaveOnly)
{
	if (!SaveOnly)
	{
		delete MENU.collis;
		memcpy_safe((void *)HOOKPOS_PlayerCollision, Collis.patch, 6);
	}
	ini->setBoolean("��������", "������", Collis.Cars, true);
	ini->setBoolean("��������", "������", Collis.Players, true);
	ini->setBoolean("��������", "�������", Collis.Objects, true);
	ini->setBoolean("��������", "���������", Collis.Buildings, true);
}

bool CollisionCheck(object_base *obj1, object_base *obj2)
{
	return (obj2->nType == 3 || obj2->nType == 2) &&
		((obj1->nType == 2 && Collis.Cars) ||
		(obj1->nType == 3 && Collis.Players) ||
		(obj1->nType == 4 && Collis.Objects));
}