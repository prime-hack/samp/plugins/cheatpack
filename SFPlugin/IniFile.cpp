#include "main.h"
void loadfl(std::vector<char> &vec, char *path)
{
	std::ifstream f(path, std::ios::binary);
	while (!f.eof())
		vec.push_back(f.get());
	vec.pop_back();
	f.close();
}
void savefl(std::vector<char> &vec, char* fname)
{
	std::ofstream f(fname, std::ios::binary);
	for (unsigned int i = 0; i < vec.size(); i++)
		f << (unsigned char)vec[i];
	f.close();
}
bool file_exist(char* file_name)
{
	WIN32_FIND_DATAA FindFileData;
	HANDLE hFindIni;

	hFindIni = FindFirstFileA(file_name, &FindFileData);
	if (hFindIni != INVALID_HANDLE_VALUE)
	{
		FindClose(hFindIni);
		return true;
	}
	FindClose(hFindIni);
	return false;
}