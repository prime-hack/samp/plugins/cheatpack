#include "main.h"
stESP_cheats cESP;
stPickups PICKUP;
void ESPInit(D3DCOLOR &color)
{
	MENU.ESP = new Menu("���������", -1, -1, 120, 150, color, -1, false);
	MENU.ESP->SetParent(MENU.gl);
	MENU.ESP->Tumblers->AddTumbler("�� ����", 5, 5, &cESP.DrawNicks);
	MENU.ESP->Tumblers->AddTumbler("������", 5, 20, &cESP.WeaponInfo);
	MENU.ESP->Tumblers->AddTumbler("���� �����", 5, 35, &cESP.ActorInfo);
	MENU.ESP->Tumblers->AddTumbler("������ ESP", 5, 50, &cESP.TrueEspBox);
	MENU.ESP->Tumblers->AddTumbler("������� ESP", 5, 65, &cESP.EasyEspBox);
	MENU.ESP->Tumblers->AddTumbler( "���������", 5, 80, &cESP.MiniMap );
	EspPick(color);
	MENU.ESP->Tumblers->AddTumbler( "������", 5, 95, MENU.submenu.PickESP->GetPresentPointer() );
	MENU.ESP->Tumblers->AddTumbler( "�����", 5, 110, &cESP.skin );

	cESP.DrawNicks = ini->esgBoolean("���������", "�� ����", false, true);
	cESP.WeaponInfo = ini->esgBoolean("���������", "������", false, true);
	cESP.ActorInfo = ini->esgBoolean("���������", "���� �����", false, true);
	cESP.TrueEspBox = ini->esgBoolean("���������", "������ ESP", false, true);
	cESP.EasyEspBox = ini->esgBoolean("���������", "������� ESP", false, true);
	cESP.MiniMap = ini->esgBoolean( "���������", "���������", false, true );
	cESP.skin = ini->esgBoolean( "���������", "�����", false, true );

	memset(cESP.PlayerShow, 0, 1004);
}

void ESPDestruct(bool SaveOnly)
{
	if (!SaveOnly)
	{
		delete MENU.submenu.PickESP;
		delete MENU.ESP;
	}
	ini->setBoolean("���������", "�� ����", cESP.DrawNicks, true);
	ini->setBoolean("���������", "������", cESP.WeaponInfo, true);
	ini->setBoolean("���������", "���� �����", cESP.ActorInfo, true);
	ini->setBoolean("���������", "������ ESP", cESP.TrueEspBox, true);
	ini->setBoolean("���������", "������� ESP", cESP.EasyEspBox, true);
	ini->setBoolean( "���������", "���������", cESP.MiniMap, true );
	ini->setBoolean( "���������", "�����", cESP.skin, true );

	ini->setBoolean("���������.������", "��������", PICKUP.bName, true);
	ini->setBoolean("���������.������", "�����", PICKUP.bID, true);
	ini->setBoolean("���������.������", "������", PICKUP.bModel, true);
	ini->setFloat("���������.������", "���������", PICKUP.fDist);
}

void EspPick(D3DCOLOR &color)
{
	PICKUP.bName = ini->esgBoolean("���������.������", "��������", false, true);
	PICKUP.bID = ini->esgBoolean("���������.������", "�����", true, true);
	PICKUP.bModel = ini->esgBoolean("���������.������", "������", false, true);
	PICKUP.fDist = ini->esgFloat("���������.������", "���������", 50.0);

	MENU.submenu.PickESP = new Menu("������", -1, -1, 170, 105, color, -1, false);
	MENU.submenu.PickESP->SetParent(MENU.gl);
	MENU.submenu.PickESP->Tumblers->AddTumbler("��������", 5, 5, &PICKUP.bName);
	MENU.submenu.PickESP->Tumblers->AddTumbler("�����", 5, 20, &PICKUP.bID);
	MENU.submenu.PickESP->Tumblers->AddTumbler("������", 5, 35, &PICKUP.bModel);
	MENU.submenu.PickESP->Elements->AddElement("���������: ", 5, 50, NULL);
	MENU.submenu.PickESP->InputBoxes->AddInputBox(115, 50, 50);
	MENU.submenu.PickESP->InputBoxes->SetInputBoxText(0, "%.1f", PICKUP.fDist);
	MENU.submenu.PickESP->Elements->AddElement("�������� ���������", 5, 65, ApplyPickDist);
}

mcall ApplyPickDist(int KeyClicked)
{
	std::string szDist = MENU.submenu.PickESP->InputBoxes->GetInputBoxText(0);
	sscanf(szDist.c_str(), "%f", &PICKUP.fDist);
	notif->AddNotification(-1, "��������� ���������� ������� �������� �� %.1f", PICKUP.fDist);
}

void PickUpEsp()
{
	if (!PICKUP.bName && !PICKUP.bID && !PICKUP.bModel)
		return;

	for (int i = 0; i < 4096; ++i)
	{
		if (!SF->getSAMP()->getInfo()->pPools->pPickup->pickup[i].iModelID)
			continue;
		if (!SF->getSAMP()->getInfo()->pPools->pPickup->pickup[i].iType)
			continue;
		if (!SF->getSAMP()->getInfo()->pPools->pPickup->IsPickupExists(i))
			continue;

		CVector Pos = *(CVector*)SF->getSAMP()->getInfo()->pPools->pPickup->pickup[i].fPosition;
		if (!world.OnScreen(Pos))
			continue;

		if (world.dist3D(Pos, world.GetCameraPosition()) > PICKUP.fDist)
			continue;
		float fScreen[2];
		SF->getGame()->convert3DCoordsToScreen(Pos.fX, Pos.fY, Pos.fZ, &fScreen[0], &fScreen[1]);
		static char szPickInfo[32];

		if (PICKUP.bName)
		{
			pFont->PrintShadow(world.get_model_name_by_id(SF->getSAMP()->getInfo()->pPools->pPickup->pickup[i].iModelID).c_str(),
				0xFF40FFFF, fScreen[0] - (pFont->DrawLength(szPickInfo) / 2), fScreen[1] - pFont->DrawHeight());
		}

		if (PICKUP.bID)
		{
			sprintf(szPickInfo, "ID: %d", i);
			pFont->PrintShadow(szPickInfo, 0xFF40FFFF, fScreen[0] - (pFont->DrawLength(szPickInfo) / 2), fScreen[1]);
		}
		if (PICKUP.bModel)
		{
			sprintf(szPickInfo, "Model: %d", SF->getSAMP()->getInfo()->pPools->pPickup->pickup[i].iModelID);
			pFont->PrintShadow(szPickInfo, 0xFF40FFFF, fScreen[0] - (pFont->DrawLength(szPickInfo) / 2), fScreen[1] + pFont->DrawHeight());
		}
	}
}

void MyActorInfo()
{
	if (!cESP.ActorInfo) return;

	static MATRIX4X4 *mat;
	static VECTOR spd;
	if (player.Driving(ACTOR_SELF))
	{
		mat = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->base.matrixStruct;
		spd = *(VECTOR*)SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->speed;
	}
	else
	{
		mat = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->base.matrixStruct;
		spd = *(VECTOR*)SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->speed;
	}
	
	static char info[128];
	sprintf(info, "Pos: %.2f %.2f %.2f\nMat: %.2f %.2f %.2f\nSpd: %.2f %.2f %.2f", mat->pos.X, mat->pos.Y, mat->pos.Z,
		-atan2f(mat->at.X, mat->at.Y), -atan2f(mat->up.X, mat->up.Y), -atan2f(mat->right.X, mat->right.Y), spd.X, spd.Y, spd.Z);
	pFont->Print(info, -1, 5, *(int*)0xC9C044 / 2 - pFont->DrawHeight()*1.5);
}

void ESP()
{
	DWORD P00L = *(int*)((*(int*)0xB74490) + 0x4);
	for (size_t hCount = 0; hCount < 35584; hCount += 0x100)
	{
		int PLAYER = *(BYTE*)P00L;
		P00L++;
		if ((PLAYER >= 0x00) && (PLAYER < 0x80))
		{
			PLAYER += hCount;
			PLAYER = SF->getSAMP()->getPlayers()->GetSAMPPlayerIDFromGTAHandle(PLAYER);
			if (player.ActorDefined(PLAYER))
			{
				struct actor_info* actor = SF->getSAMP()->getPlayers()->pRemotePlayer[PLAYER]->pPlayerData->pSAMP_Actor->pGTAEntity;
				if (actor == NULL) continue;
				CPed *ped = GAME->GetPools()->GetPed((DWORD*)actor);
				if (ped == NULL) continue;
				CVector pos = player.GetBonePosition(PLAYER, 1);
				if (ped->IsOnScreen())
				{
					//������������ �������
					EasyESP(PLAYER);
					DrawEspBox(PLAYER);
					WeaponInfo(PLAYER);
					DrawNicks( PLAYER );
				}
				MiniMAP(PLAYER);
			}
		}
	}
	PickUpEsp();
}

void MiniMAP(int &ID)
{
	static stMiniMap NickColor[1004];

	if (cESP.MiniMap)
	{
		D3DCOLOR origClr = player.GetColorRGBA(ID);
		byte RGBA[4];
		SF->getRender()->ARGB_To_A_R_G_B(origClr, RGBA[0], RGBA[1], RGBA[2], RGBA[3]);
		if (RGBA[3] < 0x80)
		{
			NickColor[ID].Changed = true;
			NickColor[ID].origClr = origClr;

			RGBA[3] = 0x80;
			player.SetColorRGBA(ID, D3DCOLOR_ARGB(RGBA[0], RGBA[1], RGBA[2], RGBA[3]));
		}
	}
	else if (NickColor[ID].Changed)
	{
		NickColor[ID].Changed = false;
		player.SetColorRGBA(ID, NickColor[ID].origClr);
	}
}

void WeaponInfo(int &ID)
{
	if (!cESP.WeaponInfo) return;
	if (SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->iAFKState) return;
	CVector pos = player.GetBonePosition(ID, 26);
	float fScreen[2];
	SF->getGame()->convert3DCoordsToScreen(pos.fX, pos.fY, pos.fZ, &fScreen[0], &fScreen[1]);
	char info[256];
	byte slot = SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->pSAMP_Actor->pGTA_Ped->weapon_slot;
	int weapon = SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->pSAMP_Actor->pGTA_Ped->weapon[slot].id;
	int ammo = SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->pSAMP_Actor->pGTA_Ped->weapon[slot].ammo_clip;
	//byte skill = SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->pSAMP_Actor->pGTA_Ped->weapon_skill;
	sprintf(info, "[%.0f/%.0f] %s\nClip: %d", weap.getDamage(weapon, /*skill+1*/2), weap.getSpeed(weapon), weap.get_name_by_id(weapon).c_str(), ammo);
	pFont->PrintShadow(info, -1, fScreen[0], fScreen[1]);
}

void DrawNicks(int &ID)
{
	if (!cESP.DrawNicks) return;
	if (SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->iAFKState) return;
	CVector pos = player.GetBonePosition(ID, 6);
	pos.fZ += 0.125;

	if ((world.dist3D(world.GetCameraPosition(), pos) >= SF->getSAMP()->getInfo()->pSettings->fNameTagsDistance) ||
		(!world.GTAfunc_IsLineOfSightClear(&world.GetCameraPosition(), &pos, 1, 0, 0, 1, 0, 0, 0)
		&& SF->getSAMP()->getInfo()->pSettings->byteNoNametagsBehindWalls) || cESP.PlayerShow[ID])
	{
		float fScreen[2];
		SF->getGame()->convert3DCoordsToScreen(pos.fX, pos.fY, pos.fZ, &fScreen[0], &fScreen[1]);
		char info[256];

		D3DCOLOR color = SF->getSAMP()->getPlayers()->GetPlayerColor(ID);
		byte RGBA[4];
		SF->getRender()->ARGB_To_A_R_G_B(color, RGBA[0], RGBA[1], RGBA[2], RGBA[3]);
		color = D3DCOLOR_ARGB(0xFF, RGBA[1], RGBA[2], RGBA[3]);

		sprintf(info, "%s {B8B8B8}({FFFFFF}%d{B8B8B8})", SF->getSAMP()->getPlayers()->GetPlayerName(ID), ID);
		pFont->PrintShadow(info, color, fScreen[0] - pFont->DrawLength(info) / 2, fScreen[1] - pFont->DrawHeight()*2);

		float HP = SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->fActorHealth;
		float AP = SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->fActorArmor;
		sprintf(info, "HP: {FFFFFF}%.0f {80FF80}| {AAAAAA}AP: {FFFFFF}%.0f", HP, AP);
		pFont->PrintShadow(info, 0xFFFF4444, fScreen[0] - pFont->DrawLength(info) / 2, fScreen[1] - pFont->DrawHeight());
	}
}

void ESP_NickShow(stRakNetHookParams *param)
{
	if (param->packetId == ScriptRPCEnumeration::RPC_ScrServerQuit)
	{
		short int playerId;
		param->bitStream->ResetReadPointer();
		param->bitStream->Read(playerId); // Id ������
		cESP.PlayerShow[playerId] = true;
	}
	if (param->packetId == ScriptRPCEnumeration::RPC_ScrShowPlayerNameTagForPlayer)
	{
		short int playerId;
		byte Show;
		param->bitStream->ResetReadPointer();
		param->bitStream->Read(playerId); // Id ������
		param->bitStream->Read(Show); //������� ����
		cESP.PlayerShow[playerId] = Show;
	}
}

void EasyESP(int &ID)
{
	if (!cESP.EasyEspBox) return;
	if (SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->iAFKState) return;

	CVector HEAD = player.GetBonePosition(ID, 6);
	CVector Leg = player.GetBonePosition(ID, 54);
	CVector oLeg = player.GetBonePosition(ID, 44);
	if (Leg.fZ > oLeg.fZ) Leg.fZ = oLeg.fZ;
	Leg.fX = HEAD.fX, Leg.fY = HEAD.fY;
	float fScreen[2];
	SF->getGame()->convert3DCoordsToScreen(HEAD.fX, HEAD.fY, HEAD.fZ, &fScreen[0], &fScreen[1]);
	float gScreen[2];
	SF->getGame()->convert3DCoordsToScreen(Leg.fX, Leg.fY, Leg.fZ, &gScreen[0], &gScreen[1]);
	if (!player.Driving(ID))
		if (world.GTAfunc_IsLineOfSightClear(&world.GetCameraPosition(), &HEAD, true, false, false, true, true, false, false))
			SF->getRender()->DrawBorderedBox(fScreen[0] - (gScreen[1] - fScreen[1]) / 3.5, fScreen[1], (gScreen[1] - fScreen[1]) / 1.75, gScreen[1] - fScreen[1], 0, 1, 0xFF00FF00);
		else SF->getRender()->DrawBorderedBox(fScreen[0] - (gScreen[1] - fScreen[1]) / 3.5, fScreen[1], (gScreen[1] - fScreen[1]) / 1.75, gScreen[1] - fScreen[1], 0, 1, 0xFFFF0000);
}

void DrawEspBox(int &ID)
{
	if (!cESP.TrueEspBox) return;

	D3DCOLOR color = 0xFF00FF00;
	CVector vec = player.GetBonePosition(ID);
	if (!world.GTAfunc_IsLineOfSightClear(&world.GetCameraPosition(), &vec, 1, 0, 0, 1, 1, 0, 0))
		color = 0xFFFF0000;

	stEspBox pos = GetPlayerBox(ID);
	SF->getRender()->DrawBorderedBox(pos.left, pos.up, pos.right, pos.down, 0, 1, color);
}

void BoneScreenMath(stEspBox &cords, int &ID, CVector &pos, POINTFLOAT &SPos, int BoneID, int BoneIDinv)
{
	pos = player.GetBonePosition(ID, BoneID);
	SF->getGame()->convert3DCoordsToScreen(pos.fX, pos.fY, pos.fZ, &SPos.x, &SPos.y);
	if ((int)SPos.x < cords.left)
	{
		cords.left = SPos.x;
		pos = player.GetBonePosition(ID, BoneIDinv);
		SF->getGame()->convert3DCoordsToScreen(pos.fX, pos.fY, pos.fZ, &SPos.x, &SPos.y);
		if ((int)SPos.x > cords.right)
			cords.right = SPos.x;
	}
	else if ((int)SPos.x > cords.right)
	{
		cords.right = SPos.x;
		pos = player.GetBonePosition(ID, BoneIDinv);
		SF->getGame()->convert3DCoordsToScreen(pos.fX, pos.fY, pos.fZ, &SPos.x, &SPos.y);
		if ((int)SPos.x < cords.left)
			cords.left = SPos.x;
	}
	pos = player.GetBonePosition(ID, BoneID);
	SF->getGame()->convert3DCoordsToScreen(pos.fX, pos.fY, pos.fZ, &SPos.x, &SPos.y);
	if ((int)SPos.y < cords.up)
	{
		cords.up = SPos.y;
		pos = player.GetBonePosition(ID, BoneIDinv);
		SF->getGame()->convert3DCoordsToScreen(pos.fX, pos.fY, pos.fZ, &SPos.x, &SPos.y);
		if ((int)SPos.y > cords.down)
			cords.down = SPos.y;
	}
	else if ((int)SPos.y > cords.down)
	{
		cords.down = SPos.y;
		pos = player.GetBonePosition(ID, BoneIDinv);
		SF->getGame()->convert3DCoordsToScreen(pos.fX, pos.fY, pos.fZ, &SPos.x, &SPos.y);
		if ((int)SPos.y < cords.up)
			cords.up = SPos.y;
	}
}

stEspBox GetPlayerBox(int &ID)
{
	stEspBox cords;
	CVector pos;
	POINTFLOAT SPos;

	cords.left = cords.up = 10000;
	cords.right = cords.down = 0;

	BoneScreenMath(cords, ID, pos, SPos, 32, 22);
	BoneScreenMath(cords, ID, pos, SPos, 22, 32);
	BoneScreenMath(cords, ID, pos, SPos, 6, 54);
	BoneScreenMath(cords, ID, pos, SPos, 54, 6);
	BoneScreenMath(cords, ID, pos, SPos, 6, 44);
	BoneScreenMath(cords, ID, pos, SPos, 44, 6);

	cords.right -= cords.left;
	cords.down -= cords.up;
	cords.left -= 6;
	cords.right += 12;
	cords.up -= 4;
	cords.down += 8;

	return cords;
}

bool CALLBACK SkinESP( CONST RECT *pSourceRect, CONST RECT *pDestRect, HWND hDestWindowOverride, CONST RGNDATA *pDirtyRegion )
{
	if ( SUCCEEDED( SF->getRender()->BeginRender() ) ) // ���� ������ ����� � ���������
	{
		if ( !SF->getGame()->isGTAMenuActive() )
		{
			DWORD P00L = *(int*)((*(int*)0xB74490) + 0x4);
			for ( size_t hCount = 0; hCount < 35584; hCount += 0x100 )
			{
				int PLAYER = *(BYTE*)P00L;
				P00L++;
				if ( (PLAYER >= 0x00) && (PLAYER < 0x80) )
				{
					PLAYER += hCount;
					PLAYER = SF->getSAMP()->getPlayers()->GetSAMPPlayerIDFromGTAHandle( PLAYER );
					if ( player.ActorDefined( PLAYER ) )
					{
						struct actor_info* actor = SF->getSAMP()->getPlayers()->pRemotePlayer[PLAYER]->pPlayerData->pSAMP_Actor->pGTAEntity;
						if ( actor == NULL ) continue;
						CPed *ped = GAME->GetPools()->GetPed( (DWORD*)actor );
						if ( ped == NULL ) continue;
						CVector pos = player.GetBonePosition( PLAYER, 1 );
						if ( ped->IsOnScreen() && cESP.skin ){
							CVector HEAD = player.GetBonePosition( PLAYER, 6 );
							if ( !world.GTAfunc_IsLineOfSightClear( &world.GetCameraPosition(), &HEAD, true, true, false, true, true, false, false ) ){
								ped->Render();
							}
						}
					}
				}
			}
		}
		SF->getRender()->EndRender(); // ��������� ���������
	}
	return true;
}