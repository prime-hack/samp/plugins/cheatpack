class CBotFarmer;

struct stOtherFunc
{
	bool AdminChecker;
	bool AdminReconnect;
	bool DestoryCar;
	bool FireCar;
	CBotFarmer *farm;
	bool farmState;
};
extern stOtherFunc other;

void OtherInit(D3DCOLOR &color);
void OtherDestruct(bool SaveOnly);

void FireCar( stInCarData& data );

class CBotFarmer : public CBotBase
{
public:
	CBotFarmer();

protected:
	virtual void Step();

private:
	int state;
	CVector pos;
};