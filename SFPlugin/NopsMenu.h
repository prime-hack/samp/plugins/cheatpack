struct Nops
{
	bool Incomming[256];
	bool Outcomming[256];
};
extern Nops *PacketNOP;
extern Nops *RPCNOP;

void CALLBACK NopsListEvent(int Select, byte key);

void NopsInit(D3DCOLOR &color);