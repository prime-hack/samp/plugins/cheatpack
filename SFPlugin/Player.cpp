#include "main.h"

bool PlayerFunc::Driving(int PlayerID)
{
	if (IsLocal(PlayerID)) return (SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->state == 50);
	else if (ActorDefined(PlayerID))
		return (SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor->pGTA_Ped->state == 50);
	return false;
};

bool PlayerFunc::isDriver(int PlayerID)
{
	if (!Driving(PlayerID)) return false;
	if (IsLocal(PlayerID)) return  (SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->passengers[0] == SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped);
	else if (ActorDefined(PlayerID))
		return (SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor->pGTA_Ped->vehicle->passengers[0] == SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor->pGTA_Ped);
	return false;
};

bool PlayerFunc::LeavingCar(int PlayerID)
{
	if (Driving(PlayerID)) return false;
	if (IsLocal(PlayerID)) return  (SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->state == 0);
	else if (ActorDefined(PlayerID))
		return (SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor->pGTA_Ped->state == 0);
	return false;
};

bool PlayerFunc::ActorDefined(int ID) //����������� � ���� ������, ��������� ����� � ���������� ������
{
	if (IsLocal(ID) || ID < 0 || ID > 1000)
		return false;
	if (SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->iIsNPC)
		return false;
	if (SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->pSAMP_Actor != 0)
		return true;
	return false;
}

bool PlayerFunc::ActorDead(int ID) //���������, ��� ����� ���� (�� ����� ���� ���������, ��� �� ���� ��� ������)
{
	if (IsLocal(ID)) return (SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->state == 55
		|| SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->state == 63);
	else if (ActorDefined(ID))
		return (SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->pSAMP_Actor->pGTA_Ped->state == 55
		|| SF->getSAMP()->getPlayers()->pRemotePlayer[ID]->pPlayerData->pSAMP_Actor->pGTA_Ped->state == 63);
	return false;
}

bool PlayerFunc::IsLocal(int ID)
{
	if (ID == ACTOR_SELF || ID == SF->getSAMP()->getPlayers()->sLocalPlayerID)
		return true;
	return false;
}

CVector PlayerFunc::GetBonePosition(int PlayerID, byte BoneID)
{
	CVector Position;
	typedef void(__thiscall *CPed__getBonePositionWithOffset) (void *_this, CVector *offset, int bodeId, bool includeAnim);
	if (PlayerID == SF->getSAMP()->getPlayers()->sLocalPlayerID || PlayerID == ACTOR_SELF)
		(CPed__getBonePositionWithOffset(0x5E01C0))(SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped, &Position, BoneID, true);
	else if (SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor != 0)
		(CPed__getBonePositionWithOffset(0x5E01C0))(SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor->pGTA_Ped, &Position, BoneID, true);
	else Position = *(CVector*)SF->getSAMP()->getStreamedOutInfo()->fPlayerPos[PlayerID];
	return Position;
};

bool PlayerFunc::inCar(int PlayerID, int CarID)
{
	if (PlayerID == ACTOR_SELF) PlayerID = SF->getSAMP()->getPlayers()->sLocalPlayerID;
	if (Driving(PlayerID))
	{
		if (isDriver(PlayerID))	return (SF->getSAMP()->getPlayers()->GetInCarData(PlayerID)->sVehicleID == CarID);
		return (SF->getSAMP()->getPlayers()->GetPassengerData(PlayerID)->sVehicleID == CarID);
	}
	return false;
};

int PlayerFunc::GetCarID(int PlayerID)
{
	if (PlayerID == ACTOR_SELF) PlayerID = SF->getSAMP()->getPlayers()->sLocalPlayerID;
	if (Driving(PlayerID))
	{
		for ( int i = 0; i < 2000; ++i )
		{
			if ( SF->getSAMP()->getVehicles()->pGTA_Vehicle[i] == nullptr )
				continue;
			vehicle_info *veh = nullptr;
			if ( PlayerID == SF->getSAMP()->getPlayers()->sLocalPlayerID )
				veh = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle;
			else veh = SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor->pGTA_Ped->vehicle;
			if ( veh == SF->getSAMP()->getVehicles()->pGTA_Vehicle[i] )
				return i;
		}
	}
	return -1;
};

void PlayerFunc::SetPosition(CVector vect)
{
	DWORD mem = 0;
	if (Driving(ACTOR_SELF))
		mem = (((DWORD)SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle) + 20);
	else mem = (((DWORD)SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped) + 20);
	mem = *(DWORD*)mem;
	mem += 48;
	*(CVector*)mem = vect;
};

bool PlayerFunc::playerIsSpawned()
{
	if (SF->getSAMP()->getPlayers()->IsPlayerDefined(SF->getSAMP()->getPlayers()->sLocalPlayerID, true))
		return (SF->getSAMP()->getPlayers()->pLocalPlayer->iIsActorAlive && !SF->getSAMP()->getPlayers()->pLocalPlayer->iIsInSpawnScreen);

	return false;
};

D3DCOLOR PlayerFunc::GetColorRGBA(int id)
{
	if (!ActorDefined(id))
		return NULL;

	D3DCOLOR	*color_table;

	color_table = (D3DCOLOR *)((uint8_t *)SF->getSAMP()->getSAMPAddr() + SAMP_COLOR_OFFSET);

	return color_table[id];
}

void PlayerFunc::SetColorRGBA(int id, D3DCOLOR RGBA)
{
	if (!ActorDefined(id))
		return;

	D3DCOLOR	*color_table;

	color_table = (D3DCOLOR *)((uint8_t *)SF->getSAMP()->getSAMPAddr() + SAMP_COLOR_OFFSET);

	color_table[id] = RGBA;
}

int PlayerFunc::SearshTargetPlayer()
{
	int RetID = -1;
	float ScreenDist = 1000.0f;
	float fScreen[2];
	DWORD P00L = *(int*)((*(int*)0xB74490) + 0x4);
	for (size_t hCount = 0; hCount < 35584; hCount += 0x100)
	{
		int PLAYER = *(BYTE*)P00L;
		P00L++;
		if ((PLAYER >= 0x00) && (PLAYER < 0x80))
		{
			PLAYER += hCount;
			PLAYER = SF->getSAMP()->getPlayers()->GetSAMPPlayerIDFromGTAHandle(PLAYER);
			if (player.ActorDefined(PLAYER))
			{
				struct actor_info* actor = SF->getSAMP()->getPlayers()->pRemotePlayer[PLAYER]->pPlayerData->pSAMP_Actor->pGTAEntity;
				if (actor == NULL) continue;
				CPed *ped = GAME->GetPools()->GetPed((DWORD*)actor);
				if (ped == NULL) continue;
				CVector pos = player.GetBonePosition(PLAYER, 1);
				if (ped->IsOnScreen())
				{
					SF->getGame()->convert3DCoordsToScreen(pos.fX, pos.fY, pos.fZ, &fScreen[0], &fScreen[1]);
					SF->getGame()->convertWindowCoordsToGame(fScreen[0], fScreen[1], &fScreen[0], &fScreen[1]);

					if (world.dist2D(fScreen[0], fScreen[1], 339.1, 179.1) < ScreenDist)
					{
						ScreenDist = world.dist2D(fScreen[0], fScreen[1], 339.1, 179.1);
						RetID = PLAYER;
					}
				}
			}
		}
	}

	return RetID;
}

int PlayerFunc::GetPlayerID(std::string nick)
{
	for (int i = 0; i < 1004; ++i)
		if (SF->getSAMP()->getPlayers()->IsPlayerDefined(i))
			if (!strcmp(SF->getSAMP()->getPlayers()->GetPlayerName(i), nick.c_str()))
				return i;
	return -1;
}

void PlayerFunc::SetMatrix(VECTOR &right, VECTOR &up, VECTOR &at)
{
	static MATRIX4X4 *mat;
	if (Driving(ACTOR_SELF))
		mat = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->base.matrixStruct;
	else if (playerIsSpawned())
	{
		mat = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->base.matrixStruct;
		SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->fCurrentRotation = -atan2f(up.X, up.Y);
		SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->fTargetRotation = -atan2f(up.X, up.Y);
	}

	mat->right = right;
	mat->up = up;
	mat->at = at;
}

void PlayerFunc::QuaternionToMatrix(float &quaterW, float &quaterX, float &quaterY, float &quaterZ, VECTOR* right, VECTOR* up, VECTOR* at)
{
	float SquarredQuaterW = 0.0f, SquarredQuaterX = 0.0f, SquarredQuaterY = 0.0f, SquarredQuaterZ = 0.0f;

	SquarredQuaterW = quaterW * quaterW;
	SquarredQuaterX = quaterX * quaterX;
	SquarredQuaterY = quaterY * quaterY;
	SquarredQuaterZ = quaterZ * quaterZ;
	right->X = SquarredQuaterX - SquarredQuaterY - SquarredQuaterZ + SquarredQuaterW;
	up->Y = SquarredQuaterY - SquarredQuaterX - SquarredQuaterZ + SquarredQuaterW;
	at->Z = SquarredQuaterZ - (SquarredQuaterY + SquarredQuaterX) + SquarredQuaterW;

	float multXY = quaterX * quaterY;
	float multWZ = quaterW * quaterZ;
	up->X = multWZ + multXY + multWZ + multXY;
	right->Y = multXY - multWZ + multXY - multWZ;

	float multXZ = quaterX * quaterZ;
	float multWY = quaterW * quaterY;
	at->X = multXZ - multWY + multXZ - multWY;
	right->Z = multWY + multXZ + multWY + multXZ;

	float multYZ = quaterY * quaterZ;
	float multWX = quaterW * quaterX;
	at->Y = multWX + multYZ + multWX + multYZ;
	up->Z = multYZ - multWX + multYZ - multWX;
}

void PlayerFunc::SetQuaternion(CVector4D quaternion)
{
	VECTOR right, up, at;
	QuaternionToMatrix(quaternion.fW, quaternion.fX, quaternion.fY, quaternion.fZ, &right, &up, &at);
	SetMatrix(right, up, at);
}

void PlayerFunc::MatrixToQuaternion(VECTOR &right, VECTOR &up, VECTOR &at, float *quaterW, float *quaterX, float *quaterY, float *quaterZ) // IDA POWA !
{
	long double v13; // st7@1
	long double v14; // st7@3
	long double v15; // st5@5
	float v16; // st6@5
	float v17; // st7@5
	long double v18; // st6@7
	int result = 0; // eax@17
	float v20; // ST28_8@17
	float v21; // ST10_8@17
	float v22; // st7@17
	float v23; // [sp+10h] [bp-20h]@7
	float v24; // [sp+18h] [bp-18h]@9
	float v25; // [sp+20h] [bp-10h]@3

	v13 = right.X + up.Y + at.Z + 1.0f;
	if (v13 < 0.0f)
		v13 = 0.0f;
	v25 = (float)sqrt(v13) * 0.5f;
	v14 = right.X + 1.0 - up.Y - at.Z;
	if (v14 < 0.0f)
		v14 = 0.0f;
	v17 = (float)sqrt(v14) * 0.5f;
	v16 = 1.0f - right.X;
	v15 = up.Y + v16 - at.Z;
	if (v15 < 0.0f)
		v15 = 0.0f;
	v23 = (float)sqrt(v15) * 0.5f;
	v18 = v16 - up.Y + at.Z;
	if (v18 < 0.0f)
		v18 = 0.0f;
	v24 = (float)sqrt(v18) * 0.5f;
	if (v25 < 0.0f)
		v25 = 0.0f;
	if (v17 < 0.0f)
		v17 = 0.0f;
	if (v23 < 0.0f)
		v23 = 0.0f;
	if (v24 < 0.0f)
		v24 = 0.0f;
	v20 = (float)_copysign(v17, at.Y - up.Z);
	v21 = (float)_copysign(v23, right.Z - at.X);
	v22 = (float)_copysign(v24, up.X - right.Y);
	*quaterW = v25;
	*quaterX = v20;
	*quaterY = v21;
	*quaterZ = v22;
}

void PlayerFunc::GetMatrix(int PlayerID, VECTOR &right, VECTOR &up, VECTOR &at)
{
	static MATRIX4X4 *mat;
	if (IsLocal(PlayerID))
	{
		if (Driving(ACTOR_SELF))
			mat = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->vehicle->base.matrixStruct;
		else if (playerIsSpawned())
			mat = SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->base.matrixStruct;
	}
	else if (ActorDefined(PlayerID))
	{
		if (Driving(PlayerID))
			mat = SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor->pGTA_Ped->vehicle->base.matrixStruct;
		else mat = SF->getSAMP()->getPlayers()->pRemotePlayer[PlayerID]->pPlayerData->pSAMP_Actor->pGTA_Ped->base.matrixStruct;
	}

	right = mat->right;
	up = mat->up;
	at = mat->at;
}

CVector4D PlayerFunc::GetQuaternion(int PlayerID)
{
	VECTOR right, up, at;
	GetMatrix(PlayerID, right, up, at);
	CVector4D RET;
	MatrixToQuaternion(right, up, at, &RET.fW, &RET.fX, &RET.fY, &RET.fZ);

	return RET;
}