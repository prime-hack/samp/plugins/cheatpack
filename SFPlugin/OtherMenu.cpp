#include "main.h"

void CALLBACK AdminRecon( int state )
{
	if ( !state )
		return;
	
	if ( !AdminCheckerInfo.admins_connect.empty() ){
		AdminCheckerInfo.admins_connect.clear();
		Reconnect( 1 );
	}
}

void CALLBACK Farmer( int state )
{
	if ( state )
		other.farm = new CBotFarmer();
	else delete other.farm;
}

void OtherInit(D3DCOLOR &color)
{
	MENU.Other = new Menu("������", -1, -1, 120, 130, color, -1, false);
	MENU.Other->SetParent(MENU.gl);
	MENU.Other->Tumblers->AddTumbler( "����������", 5, 5, &other.AdminChecker );
	//MENU.Other->Tumblers->AddTumbler( "����� �����", 5, 20, &other.AdminReconnect );
	int idAR = MENU.Other->Elements->AddElement( "����� �����", 5, 20, AdminRecon );
	MENU.Other->Elements->SetTumblerMode( idAR, &other.AdminReconnect );
	MENU.Other->Tumblers->AddTumbler( "��� ���", 5, 35, &other.DestoryCar );
	MENU.Other->Tumblers->AddTumbler( "������ �����", 5, 50, &other.FireCar );

	//int idF = MENU.Other->Elements->AddElement( "������", 5, 65, Farmer );
	//MENU.Other->Elements->SetTumblerMode( idF, &other.farmState );

	other.AdminChecker = ini->esgBoolean( "������", "����������", false, true );
	other.AdminReconnect = ini->esgBoolean( "������", "����� �����", false, true );
	other.DestoryCar = ini->esgBoolean( "������", "��� ���", false, true );
	other.FireCar = ini->esgBoolean( "������", "������ �����", false, true );
}

void OtherDestruct(bool SaveOnly)
{
	if (!SaveOnly)
	{
		delete MENU.Other;
	}
	ini->setBoolean( "������", "����������", other.AdminChecker, true );
	ini->setBoolean( "������", "����� �����", other.AdminReconnect, true );
	ini->setBoolean( "������", "��� ���", other.DestoryCar, true );
	ini->setBoolean( "������", "������ �����", other.FireCar, true );
}

void FireCar( stInCarData& data )
{
	if ( !other.FireCar )
		return;

	int ID = data.sVehicleID;
	static int counter = 0;
	if ( counter < 10 && SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID]->pGTA_Vehicle->m_nVehicleFlags[0] != 8 ){
		SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID]->pGTA_Vehicle->hitpoints = 249.0f;
		data.fVehicleHealth = 249;
		++counter;
	}
	else {
		SF->getSAMP()->getVehicles()->pSAMP_Vehicle[ID]->pGTA_Vehicle->hitpoints = 1000;
		data.fVehicleHealth = 1000;
		counter = 0;
	}
}


CBotFarmer::CBotFarmer() : CBotBase()
{
	state = 0;
	pos = { 0.0, 0.0, 0.0 };
}

void CBotFarmer::Step()
{
	notif->AddNotification( -1, "state %d", state );
	if ( state == 0 ){
		std::regex re( "���������" );
		pos = Get3DText( 150.0, re );
		RotateTo( pos );
		if ( GoTo( pos ) )
			state = 1;
	}
	else if ( state == 1 ){
		pos = GetMarker( 150.0f );
		if ( pos.fX == 0.0f && pos.fY == 0.0f && pos.fZ == 0.0f ){
			state = 0;
			return;
		}
		if ( GoTo( pos ) )
			state = 2;
	}
	else if ( state == 2 ){
		pos = GetMarker( 10.0f );
		if ( pos.fX == 0.0f && pos.fY == 0.0f && pos.fZ == 0.0f )
			state = 0;
	}
	Wait( 500 );
}