#include "main.h"
int iTimeRecon = 15;

template <typename T>
union byteValue{
	T value;
	byte bytes[sizeof( T )];
};

void UdpThread( std::string ip, byte ip_0, byte ip_1, byte ip_2, byte ip_3, WORD port )
{
	WSADATA wsaData;
	SOCKET SendRecvSocket;  // ����� ��� ������ � ��������
	sockaddr_in ServerAddr;  // ��� ����� ����� ������� � ��������
	char query[64];  // ����� ��������

	// Initialize Winsock
	WSAStartup( MAKEWORD( 2, 2 ), &wsaData );

	// Create a SOCKET for connecting to server
	SendRecvSocket = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );

	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_addr.s_addr = inet_addr( ip.c_str() );
	ServerAddr.sin_port = htons( port );

	connect( SendRecvSocket, (struct sockaddr *)&ServerAddr, sizeof( sockaddr ) );

	query[0] = 'S';
	query[1] = 'A';
	query[2] = 'M';
	query[3] = 'P';

	query[4] = ip_0;
	query[5] = ip_1;
	query[6] = ip_2;
	query[7] = ip_3;
	byteValue<WORD> _port;
	_port.value = port;
	query[8] = _port.bytes[0];
	query[9] = _port.bytes[1];
	query[11] = '\0';

	// ���������� ������ �� ������
	for ( int i = 0; i < 3; ++i ){
		query[10] = 'p';
		sendto( SendRecvSocket, query, strlen( query ), 0, (sockaddr *)&ServerAddr, sizeof( ServerAddr ) );
		query[10] = 'i';
		sendto( SendRecvSocket, query, strlen( query ), 0, (sockaddr *)&ServerAddr, sizeof( ServerAddr ) );
		query[10] = 'r';
		sendto( SendRecvSocket, query, strlen( query ), 0, (sockaddr *)&ServerAddr, sizeof( ServerAddr ) );
		Sleep( 250 );
	}

	closesocket( SendRecvSocket );
	WSACleanup();
}

DWORD lastTime = 16000;
void RejTimer()
{
	static bool UdpReconnect = false;

	if ( ConnectInfo.state != 0 )
		return;
	if (ConnectInfo.dwTimer == 0)
		return;
	if ( ConnectInfo.dwTimer - GetTickCount() < 5000 && !UdpReconnect ){
		char newIp[64];
		WSADATA wsaData;
		WSAStartup( MAKEWORD( 1, 1 ), &wsaData );
		struct hostent *he = gethostbyname( SF->getSAMP()->getInfo()->szIP );
		if ( he != NULL ){
			strcpy( newIp, inet_ntoa( *((struct in_addr *) he->h_addr_list[0]) ) );
		}
		WSACleanup();

		byte ip_0 = 127;
		byte ip_1 = 0;
		byte ip_2 = 0;
		byte ip_3 = 1;
		if ( sscanf(newIp, "%d.%d.%d.%d", &ip_0, &ip_1, &ip_2, &ip_3) ){
			std::thread thr( UdpThread, std::string( newIp ), ip_0, ip_1, ip_2, ip_3, SF->getSAMP()->getInfo()->ulPort );
			thr.detach();
			UdpReconnect = true;
		}
	}
	if ( lastTime - (ConnectInfo.dwTimer - GetTickCount()) >= 1000 && lastTime - (ConnectInfo.dwTimer - GetTickCount()) < iTimeRecon * 1000){
		notif->AddNotification( -1, "��������� %s:%d: %d���.", SF->getSAMP()->getInfo()->szIP,
								SF->getSAMP()->getInfo()->ulPort, (ConnectInfo.dwTimer - GetTickCount()) / 1000 );
		lastTime = ConnectInfo.dwTimer - GetTickCount();

		if (player.playerIsSpawned() && !player.ActorDead(-1)) {
			SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->hitpoints = 1000;
			SF->getSAMP()->getPlayers()->pLocalPlayer->pSAMP_Actor->pGTA_Ped->armor = 0;
		}
	}
	if (ConnectInfo.dwTimer > GetTickCount())
		return;
	AdminCheckerInfo.admins_connect.clear();
	SF->getSAMP()->getInfo()->RestartGame();
	SF->getSAMP()->getInfo()->iGameState = GAMESTATE_WAIT_CONNECT;
	ConnectInfo.dwTimer = 0;
	UdpReconnect = false;
}
void CALLBACK Reconnect(int KeyClicked)
{
	if ( !ConnectInfo.dwTimer ){
		ConnectInfo.dwTimer = iTimeRecon * 1000 + GetTickCount() + 100;
		lastTime = iTimeRecon * 1000 + 1000;
	}

	if ( !ConnectInfo.autoconnect && KeyClicked == 1337 )
		return;

	std::string nick = MENU.connects->InputBoxes->GetInputBoxText(0);
	if (nick.length() > 3 && nick.length() < 26)
		SF->getSAMP()->getPlayers()->SetLocalPlayerName( nick.c_str() );
	SF->getSAMP()->disconnect( 100 );
	ConnectInfo.state = 0;
}
void CALLBACK Autoconnect( int state )
{
	if ( !state )
		return;
	Reconnect( 1337 );
}
void CALLBACK SrvList(int Element, byte Key)
{
	ConnectInfo.dwTimer = 0;
	char newIp[64];
	WSADATA wsaData;
	WSAStartup( MAKEWORD( 1, 1 ), &wsaData );
	struct hostent *he = gethostbyname( ConnectInfo.srv[Element].servIP );
	if ( he != NULL ){
		strcpy( newIp, inet_ntoa( *((struct in_addr *) he->h_addr_list[0]) ) );
	}
	WSACleanup();

	byte ip_0 = 127;
	byte ip_1 = 0;
	byte ip_2 = 0;
	byte ip_3 = 1;
	if ( sscanf( newIp, "%d.%d.%d.%d", &ip_0, &ip_1, &ip_2, &ip_3 ) ){
		std::thread thr( UdpThread, std::string( newIp ), ip_0, ip_1, ip_2, ip_3, ConnectInfo.srv[Element].port );
		thr.detach();
	}
	std::string nick = MENU.connects->InputBoxes->GetInputBoxText(0);
	if (nick.length() > 3 && nick.length() < 26)
		SF->getSAMP()->getPlayers()->SetLocalPlayerName(nick.c_str());
	((void(__cdecl *)(unsigned))(SF->getSAMP()->getSAMPAddr() + 0x1BC20))(ConnectInfo.srv[Element].port);
	SF->getSAMP()->disconnect(1);
	sprintf(SF->getSAMP()->getInfo()->szIP, "%s\x00", ConnectInfo.srv[Element].servIP);
	SF->getSAMP()->getInfo()->ulPort = ConnectInfo.srv[Element].port;
	AdminCheckerInfo.admins_connect.clear();
	SF->getSAMP()->getInfo()->iGameState = 13;
	SF->getSAMP()->getInfo()->RestartGame();
	SF->getSAMP()->getInfo()->iGameState = 9;
}
void ConnectsInit(D3DCOLOR &color)
{
	MENU.connects = new Menu("�����������", -1, -1, 460, 200, color, -1, false);
	MENU.connects->SetParent(MENU.gl);
	MENU.connects->Elements->AddElement("���:", 5, 5, NULL);
	MENU.connects->InputBoxes->AddInputBox(50, 5, 320);
	//MENU.connects->Tumblers->AddTumbler( "�����������", 358, 30, &ConnectInfo.autoconnect );
	int idAC = MENU.connects->Elements->AddElement( "�����������", 358, 30, Autoconnect );
	MENU.connects->Elements->SetTumblerMode( idAC, &ConnectInfo.autoconnect );
	MENU.connects->InputBoxes->SetInputBoxText(0, SF->getSAMP()->getPlayers()->GetPlayerName(SF->getSAMP()->getPlayers()->sLocalPlayerID));
	MENU.connects->Elements->AddElement("��������:", 5, 30, NULL);
	MENU.connects->Sliders->AddSlider(95, 32, 220, 15, &iTimeRecon, dINT, 1.0, 60.0);
	MENU.connects->Elements->AddElement("���������", 375, 5, Reconnect);
	MENU.connects->Listings->AddListing(5, 55, 450, 120, SrvList);

	ConnectInfo.autoconnect = ini->esgBoolean("�����������", "�����������", false, true);

	char filePath[256];// , User[64];
	/*DWORD size = 64;
	GetUserNameA(User, &size);*/
	//sprintf(filePath, "C:\\Users\\%s\\Documents\\GTA San Andreas User Files\\SAMP\\USERDATA.DAT", User);
	SHGetSpecialFolderPath(0, filePath, CSIDL_PERSONAL, 0);
	strcat(filePath, "\\GTA San Andreas User Files\\SAMP\\USERDATA.DAT");
	//SF->Log(filePath);
	if (file_exist(filePath))
	{
		std::vector<char> userdat;
		loadfl(userdat, filePath); //servers +12
		DWORD ServCnt = *(DWORD*)(userdat.data() + 8);
		DWORD point = 12;
		for (size_t i = 0; i < ServCnt; i++)
		{
			Server srv;
			memset(srv.servIP, 0, 256);
			srv.port = 0;
			DWORD len = 0;
			char servName[256] = { 0 };

			len = *(DWORD*)(userdat.data() + point);
			point += 4;
			memcpy(srv.servIP, (void*)(userdat.data() + point), len);
			point += len;
			srv.port = *(DWORD*)(userdat.data() + point);
			point += 4;
			len = *(DWORD*)(userdat.data() + point);
			point += 4;
			memcpy(servName, (void*)(userdat.data() + point), len);
			point += len;
			len = *(DWORD*)(userdat.data() + point);
			point += 4;
			point += len;
			len = *(DWORD*)(userdat.data() + point);
			point += 4;
			point += len;

			ConnectInfo.srv.push_back(srv);
			MENU.connects->Listings->AddTextOnListing(0, servName);
		}
	}
	else SF->Log("ERROR: Can't find file \"USERDATA.DAT\".");
}
void ConnectDestruct(bool SaveOnly)
{
	if (!SaveOnly)
	{
		delete MENU.connects;
	}
	ini->setBoolean("�����������", "�����������", ConnectInfo.autoconnect, true);
}