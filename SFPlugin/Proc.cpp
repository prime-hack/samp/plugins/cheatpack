#include "main.h"

void ScreenFont()
{
	int fSize = 8;
	static bool FontDef = false;
	if (FontDef) SF->getRender()->ReleaseFont(pFont);
	else FontDef = true;
	int res = *(int*)0xC9C040;
	if (res <= 640) fSize = 6;
	if (res <= 1200) fSize = 7;
	if (res >= 1300 && res < 1800) fSize = 9;
	if (res >= 1800) fSize = 10;
	pFont = SF->getRender()->CreateNewFont("Tahoma", fSize, 13);
}

bool __stdcall lTimer(UINT time)
{
	static std::vector<struct stlTimer> vec;
	void *addr = _ReturnAddress();
	time += GetTickCount();

	int id = 0; //�����/���������� ������� � "����"
	if (vec.size())
	{
		for (id = 0; id < vec.size(); id++)
			if (vec[id].addr == addr)
				break;
		if (vec[id].addr != addr)
		{
			vec.push_back({ addr, time, true });
			id++;
		}
	}
	else vec.push_back({ addr, time, true });

	if (!vec[id].pass) //��������� ������ �������
	{
		vec[id].time = time;
		vec[id].pass = true;
		return false;
	}
	else if (vec[id].time <= GetTickCount()) //����������� ��������
	{
		vec[id].pass = false;
		return true;
	}

	return false;
}

void *memcpy_safe(void *_dest, const void *_src, size_t stLen)
{
	if (_dest == nullptr || _src == nullptr || stLen == 0)
		return nullptr;

	MEMORY_BASIC_INFORMATION	mbi;
	VirtualQuery(_dest, &mbi, sizeof(mbi));
	VirtualProtect(mbi.BaseAddress, mbi.RegionSize, PAGE_EXECUTE_READWRITE, &mbi.Protect);

	void	*pvRetn = memcpy(_dest, _src, stLen);
	VirtualProtect(mbi.BaseAddress, mbi.RegionSize, mbi.Protect, &mbi.Protect);
	FlushInstructionCache(GetCurrentProcess(), _dest, stLen);
	return pvRetn;
}

void Str2Bytes(std::string &str, byte *mas)
{
	size_t len = str.length();
	char text[256] = { 0 }, CT[256] = {0};

	if (len % 2 != 0)
	{
		sprintf(text, "0%s", str.c_str());
		len++;
	}
	else sprintf(text, str.c_str());

	for (int i = 0; i < len / 2; i++)
	{
		memcpy(CT, (char*)(text + i * 2), 2);
		sscanf(CT, "%2X", &mas[i]);
	}
}

int GTAfunc_isModelLoaded(int iModelID)
{
	int *ModelsLoadStateArray = (int *)0x8E4CD0;
	return ModelsLoadStateArray[5 * iModelID];
}

void GTAfunc_requestModelLoad(int iModelID)
{
	if (iModelID < 0)
		return;
	__asm
	{
		push 2
			push iModelID
			mov edx, FUNC_RequestModel
			call edx
			pop edx
			pop edx
	}
}

void GTAfunc_loadRequestedModels(void)
{
	uint32_t	func_load = FUNC_LoadAllRequestedModels;
	__asm
	{
		push 0
			call func_load
			add esp, 4
	}
}