#include "main.h"

Queue::Queue()
{
	data = NULL;
	prev = NULL;
	next = NULL;
}

Queue::~Queue()
{
	delete data;
	prev->next = next;
	next->prev = prev;
}

void Queue::push(Queue *after, LPVOID data)
{
	if (after != NULL)
	{
		next = after;
		after->prev = this;
	}
	this->data = data;
}

LPVOID Queue::pop()
{
	return data;
}

bool Queue::IsEmpty()
{
	if (data != NULL || next != NULL || prev != NULL)
		return false;
	return true;
}