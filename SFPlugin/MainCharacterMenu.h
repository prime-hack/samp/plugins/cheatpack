struct stGodMode
{
	bool Bullet;
	bool Fire;
	bool Collision;
	bool Melee;
	bool Explosion;
	float fPlayerHealth = 100.0;
	//Car
	bool cBullet;
	bool cFire;
	bool cCollision;
	bool cMelee;
	bool cExplosion;
	float fVehicleHealth = 1000.0;
};
extern stGodMode GM;

struct stAditionalCharacterCheat
{
	bool bunnyhop;
	bool GodMode;
	bool Acceleration;
	bool Bug_C;
	bool AntiFall;
	bool DMZZ;
};
extern stAditionalCharacterCheat ACC;

void mCharacterInit(D3DCOLOR &color);
void PlayerGM_Init(D3DCOLOR &color);
void CarGM_Init(D3DCOLOR &color);

void mCharacterDestruct(bool SaveOnly);