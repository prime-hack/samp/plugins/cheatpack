#include "main.h"
stTeleports teleport;

void InitTeleports()
{
	teleport.Activate = keyb.GetKeyNum(ini->esgString("��������", "���������", "mbutton"));
	teleport.CW_TP = keyb.GetKeyNum(ini->esgString("��������", "�� �� ������", "lbutton"));
	teleport.MAP_TP = keyb.GetKeyNum(ini->esgString("��������", "�� �� �����", "rbutton"));
}

void ClickWarp()
{
	static bool bClickWarp = false;
	if (SF->getGame()->isKeyPressed(teleport.Activate))
	{
		bClickWarp ^= true;
		if (SF->getSAMP()->getMisc()->iCursorMode)
			keyb.toggleCursor(false);
	}

	if (bClickWarp)
	{
		if (!SF->getSAMP()->getMisc()->iCursorMode) keyb.toggleCursor(true);

		if (SF->getGame()->isKeyPressed(teleport.MAP_TP))
		{
			bClickWarp = false;
			keyb.toggleCursor(false);
			CVector POS = world.MarkerPos();
			if (POS.fX != 0.0 || POS.fY != 0.0)
				player.SetPosition(POS);
		}

		float MPZ = 0.0;
		if (FindCWObject(MPZ, 400.0, 6.0))
			if (FindCWObject(MPZ, MPZ + 6.0, 0.35))
				if (FindCWObject(MPZ, MPZ + 0.35, 0.005))
				{
					POINT MousePosition = SF->getGame()->getCursorPos();
					CVector POS = { 0.0, 0.0, 0.0 };
					SF->getGame()->convertScreenCoordsTo3D(MousePosition.x, MousePosition.y, MPZ, &POS.fX, &POS.fY, &POS.fZ);
					float X, Y;
					POS.fZ += 1.0f;
					SF->getGame()->convert3DCoordsToScreen(POS.fX, POS.fY, POS.fZ - 1.0, &X, &Y);
					char ddist[64];
					sprintf(ddist, "%.2f", world.dist3D(POS, world.GetCameraPosition()));
					pFont->Print(ddist, -1, MousePosition.x, MousePosition.y - pFont->DrawHeight());
					if (SF->getGame()->isKeyPressed(teleport.CW_TP))
					{
						player.SetPosition(POS);
						SF->getGame()->convertScreenCoordsTo3D(MousePosition.x, MousePosition.y, MPZ + 0.01, &POS.fX, &POS.fY, &POS.fZ);
						bClickWarp = false;
						keyb.toggleCursor(false);
						if (!world.GTAfunc_IsLineOfSightClear(&world.GetCameraPosition(), &POS, false, player.Driving(ACTOR_SELF) ^ true, false, false, false, false, false))
						{
							int CarID = world.FindCar(POS);
							if (SF->getSAMP()->getVehicles()->pSAMP_Vehicle[CarID]->pGTA_Vehicle->door_status != 2)
							{
								SF->getSAMP()->sendEnterVehicle(CarID, 0);
								SF->getCLEO()->callOpcode("072A: put_actor $PLAYER_ACTOR into_car %d driverseat", SF->getSAMP()->getVehicles()->GetCarHandleFromSAMPCarID(CarID));
							}
						}
					}
				}
	}
}

bool FindCWObject(float &MPZ, float depth, float FindStep)
{
	POINT MousePosition = SF->getGame()->getCursorPos();
	CVector POS = { 0.0, 0.0, 0.0 };

	while (MPZ < depth)
	{
		SF->getGame()->convertScreenCoordsTo3D(MousePosition.x, MousePosition.y, MPZ, &POS.fX, &POS.fY, &POS.fZ);

		if (!world.GTAfunc_IsLineOfSightClear(&world.GetCameraPosition(), &POS, true, true, false, true, true, false, false))
		{
			MPZ -= FindStep;
			return true;
		}
		else MPZ += FindStep;
	}

	return false;
}